<?php

namespace Avantel;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = ['name', 'lastname', 'cedula', 'address'];

    /* Establecemos las relaciones que tengan */
}
