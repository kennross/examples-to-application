@extends('layouts.admin')

@section('title-page')
Mi Index
@endsection

@section('main-content')
<section class="content-header">
	<h1>
		Detalle de Usuario
	</h1>
	<ol class="breadcrumb">
		<li><a href="#">
			<i class="fa fa-dashboard"></i> 
			Inicio
		</a>
	</li>	
	<li class="active">
		Detalle de Usuario
	</li>
</ol>
</section>

<section class="content">

	@include('layouts.alerts.success')
	<div class="box box-primary">
		<div class="box-body box-profile">
			<img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">

			<h3 class="profile-username text-center">
				{{$user->name . ' ' . $user->lastname}}
			</h3>

			<p class="text-muted text-center">
				{{$user->cedula}}
			</p>		

			<form method="POST" action="{{URL::to('users/' . $user->id)}}"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="{{csrf_token()}}">
				<input class="btn btn-block btn-danger" type="submit" value="Eliminar">
			</form>				
		</div>
		<!-- /.box-body -->
	</div>
</section>
@endsection