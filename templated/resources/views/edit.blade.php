@extends('layouts.admin')

@section('title-page')
Mi Index
@endsection

@section('main-content')
<section class="content-header">
	<h1>
		Editat Usuario
	</h1>
	<ol class="breadcrumb">
		<li><a href="#">
			<i class="fa fa-dashboard"></i> 
			Inicio
		</a>
	</li>	
	<li class="active">
		Editat Usuario
	</li>
</ol>
</section>

<section class="content">
	<!-- Default box -->
	@include('layouts.alerts.success')
	<div class="box">		
		<div class="box-body">			
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">

					<form method="POST" action="{{URL::to('users/' . $user->id)}}" accept-charset="UTF-8" enctype="multipart/form-data"><input name="_method" type="hidden" value="PUT"><input name="_token" type="hidden" value="{{csrf_token()}}">

					<input type="hidden" name="id" value="{{$user->id}}">

						<div class="form-group">
							<label for="name">
								Nombre
							</label>
							<input type="text" name="name" class="form-control" placeholder="Nombre" value="{{$user->name}}">
						</div>

						<div class="form-group">
							<label for="lastname">
								Apellido
							</label>
							<input type="text" name="lastname" class="form-control" placeholder="Nombre" value="{{$user->lastname}}">
						</div>

						<div class="form-group">
							<label for="cedula">
								Cédula
							</label>
							<input type="text" name="cedula" class="form-control" placeholder="Nombre" value="{{$user->cedula}}">
						</div>

						<div class="form-group">
							<label for="address">
								Dirección
							</label>
							<input type="text" name="address" class="form-control" placeholder="Nombre" value="{{$user->address}}">
						</div>

						<button class="btn btn-block btn-primary">
							Actualizar Persona
						</button>
						
					</form>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
@endsection