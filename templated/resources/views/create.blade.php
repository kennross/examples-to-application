@extends('layouts.admin')

@section('title-page')
Mi Index
@endsection

@section('main-content')
<section class="content-header">
	<h1>
		Listado de Usuarios		
	</h1>
	<ol class="breadcrumb">
		<li><a href="#">
			<i class="fa fa-dashboard"></i> 
			Inicio
		</a>
	</li>	
	<li class="active">
		Listado de Usuarios
	</li>
</ol>
</section>

<section class="content">
	<!-- Default box -->
	@include('layouts.alerts.success')
	<div class="box">		
		<div class="box-body">			
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">
					<form action="{{URL::to('users')}}" method="POST">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-group">
							<label for="name">
								Nombre
							</label>
							<input type="text" name="name" class="form-control" placeholder="Nombre">
						</div>

						<div class="form-group">
							<label for="lastname">
								Apellido
							</label>
							<input type="text" name="lastname" class="form-control" placeholder="Nombre">
						</div>

						<div class="form-group">
							<label for="cedula">
								Cédula
							</label>
							<input type="text" name="cedula" class="form-control" placeholder="Nombre">
						</div>

						<div class="form-group">
							<label for="address">
								Dirección
							</label>
							<input type="text" name="address" class="form-control" placeholder="Nombre">
						</div>

						<button class="btn btn-block btn-primary">
							Registrar Persona
						</button>
						
					</form>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
@endsection