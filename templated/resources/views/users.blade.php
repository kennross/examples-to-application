@extends('layouts.admin')

@section('title-page')
Mi Index
@endsection

@section('main-content')
<section class="content-header">
	<h1>
		Listado de Usuarios
	</h1>
	<ol class="breadcrumb">
		<li><a href="#">
			<i class="fa fa-dashboard"></i> 
			Inicio
		</a>
	</li>	
	<li class="active">
		Listado de Usuarios
	</li>
</ol>
</section>

<section class="content">
	
	@include('layouts.alerts.success')

	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">
				Acciones de los usuarios
			</h3>
		</div>
		<div class="box-body">			
			<table class="table table-bordered">
				<tr>
					<th>
						Nombre
					</th>
					<th>
						Apellido
					</th>
					<th>
						Dirección
					</th>
					<th>
						Cédula
					</th>
					<th>
						Fecha Creación
					</th>
					<th width="200px">
						Acciones
					</th>
				</tr>
				@if (sizeof($users) == 0)
				<tr>
					<td class="lead text-center" colspan="6">
						No existen usuarios registrados
					</td>
				</tr>
				@else
				@foreach($users as $u)
				<tr>
					<td>
						{{$u->name}}
					</td>
					<td>
						{{$u->lastname}}
					</td>
					<td>
						{{$u->address}}
					</td>
					<td>
						{{$u->cedula}}
					</td>
					<td>
						{{$u->created_at->diffForHumans()}}
					</td>
					<td>
						<a class="btn btn-xs btn-success" href="{{URL::to('users/' . $u->id)}}">
							<i class="fa fa-link"></i> 
							Ver en detalle
						</a>
						<a class="btn btn-xs btn-success" href="{{URL::to('users/' . $u->id . '/edit')}}">
							<i class="fa fa-link"></i> 
							Actualizar
						</a>
					</td>
				</tr>
				@endforeach
				@endif
			</table>
			{!!$users->render()!!}
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
@endsection