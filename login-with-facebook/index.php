<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		Inicio de Sesión
	</title>
	<link rel="stylesheet" href="http://localhost/test/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://localhost/test/assets/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="http://localhost/test/assets/css/sweetalert.css">
	<link rel="stylesheet" href="http://localhost/test/assets/css/toastr.min.css">
</head>
<body>
<div id="fb-root"></div>

	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId: '1983506171872755',
				xfbml: true,
				version: 'v2.9'
			});
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/es_LA/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>


	<div class="container">
		<br>
		<div class="panel panel-default">
			<div class="panel-body text-center">
				<button class="btn btn-primary" id="IngresaFacebook" onclick="ingresar()">
					Ingresar
				</button>

				<div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
			</div>
		</div>
	</div>


	<script src="http://localhost/test/assets/js/jQuery-2.1.4.min.js"></script>
	<script src="http://localhost/test/assets/js/bootstrap-select.min.js"></script>
	<script src="http://localhost/test/assets/js/bootstrap.min.js"></script>
	<script src="http://localhost/test/assets/form-validator/jquery.form-validator.min.js"></script>
	<script src="http://localhost/test/assets/js/moment-with-locales.js"></script>
	<script src="http://localhost/test/assets/js/toastr.min.js"></script>
	<script src="http://localhost/test/assets/js/sweetalert.min.js"></script>
	<script src="http://localhost/test/assets/js/vue.js"></script>
	<script src="http://localhost/test/assets/js/config.js"></script>
	<script src="http://localhost/test/assets/js/util.js"></script>


	
	<script>
		function validarUsuario() {  
			FB.getLoginStatus(function(response) {  
				if(response.status == 'connected') {
					FB.api('/me?fields=id,name,email,gender', function(response) {
						console.log('Me alegro de verte, ' + response.email + '.');
						console.log('Hola ' + response.name);
						console.log(response)  ;
					});
				} else if(response.status == 'not_authorized') {  
					alert('Debes autorizar la app!');  
				} else {  
					alert('Debes ingresar a tu cuenta de Facebook!');  
				}  
			});  
		}  

		function ingresar() {  
			FB.login(function(response){  
				validarUsuario();  
			}, {scope: 'public_profile, email'});  
		}
	</script>
</body>
</html>