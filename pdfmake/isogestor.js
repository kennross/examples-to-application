
document.addEventListener("DOMContentLoaded", function(event) {
	function toDataURL(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.responseType = 'blob';
		xhr.send();
	}

	var logo_company = null;

	toDataURL('lteam.jpg', function(dataUrl) {
		logo_company = dataUrl;

		var doc = {
			content: [
			/* Detail Image One, Data School and Period Academic and Image two */
			{
				columns: [
				/* Data application, people and company */
				{
					width: '*',
					alignment: 'left',
					stack: [
					{
						style: 'h1',
						text: 'ISOGestor'
					},
					{
						style: 'h2',
						text: 'Política y Objetivos'
					},
					{
						style: 'h2sub',
						text: ['Realizado por ', {text: 'Kennit Romero', style: 'user'}, ' de la empresa ',{text: 'Lteam', style: 'company'}]
					}
					]
				},
				/* Image Company */
				{
					width: 'auto',
					stack: [
					{
						image: logo_company,
						width: 64,
						heigth: 64
					}
					]
				},
				],
			},
			/* Line divider */
			{
				table: {
					widths: ['*'],
					body: [[""], [""]],
					style: 'linedivider'
				},
				layout: {
					hLineWidth: function(i, node) {
						return (i === 0 || i === node.table.body.length) ? 0 : 0.8;
					},
					vLineWidth: function(i, node) {
						return 0;
					},
					hLineColor: function(i, node) {
						return '#ccc';
					}
				}
			},
			{
			text: [
				'Fecha de realización ',
				{ text: 'Julio 13, 2017 ', bold: true },
			]
		},
			],
			styles: {
				h1: {
					fontSize: 25,
					bold: true
				},
				h2: {
					fontSize: 20,
					bold: true
				},
				h2sub: {
					margin: [0, 0, 0, 0],
					fontSize: 15
				},
				user: {
					color: '#aaa'
				},
				company: {
					color: '#0275d8'
				},
				linedivider: {
					margin: [0, 50, 0, 15],
					borderLines: [[0.1,0.1]],
					borderCols:[[0.1,0.1]],
					// color: '#aaa'
				}
			},
			defaultStyle: {
				font: 'Roboto'
			},
			pageOrientation: 'landscape',
			pageSize: 'FOLIO',
		}
		var pdf = pdfMake.createPdf(doc);
		pdf.open();
	});

});
// pdf.download();

