var doc = {
	content: [
	/* Detail Image One, Data School and Period Academic and Image two */
	{
		columns: [
		{
			width: '*',
			alignment: 'left',
			stack: [
			{
				style: 'h1',
				text: 'COLEGIO MILITAR CALDAS'
			},
			{
				style: 'h2',
				text: 'BOLETÍN INFORMATIVO PRIMER SEMESTRE 2017'
			},
			{
				style: 'h3',
				text: 'RESOLUCION DEL COLEGIO VAMOS A PONER ESTA MIENTRAS TANTO'
			}
			]
		},
		/* Image to 'Green School'*/
		{
			width: 'auto',
			stack: [
			{
				image: green_school_img_data,
				width: 64,
				heigth: 62
			}
			]
		},
		],
	},
	/* Data Student and Group */
	{
		columns: [
		{
			style: 'subheader',
			text: [
			{ text: 'Estudiante ', fontSize: 10, bold: true },
			{ text: 'RUZ ROMERO KENNIT DAVID ', fontSize: 10, bold: false }
			]
		},
		{
			style: 'subheader',
			text: [
			{ text: 'Curso ', fontSize: 10, bold: true },
			{ text: 'Transición ', fontSize: 10 }
			]
		},
		]
	},
	/* Data Note */
	{
		style: 'tableStyle',
		layout: 'lightHorizontalLines', // optional
		table: {
			widths: ['*', 'auto', 'auto', 'auto', 'auto'],
			body: [
			/* Row Header Table - Area */
			[{
				text: 'CIENCIAS NATURALES Y EDUCACIÓN AMBIENTAL', 
				colSpan: 4, 
				style: 'tableHeader'
			},
			{'': ''},{'': ''},{'': ''},
			{
				text: 'Definitiva', 
				style: 'tableHeader',
				alignment: 'right', 
			},
			],

			// /* Row SubHeader Table - Subject */
			[{
				text: 'Asignatura: FÍSICA', 
				style: 'subject', 
				colSpan: 3
			}, 
			{'': ''},  {'':''},
			{
				text: 'IH: 3', 
				style: 'subject'
			}, 
			{
				text: '72 DB', 
				style: 'subject', 
				alignment: 'right', 
				bold: true, 
				color: 'green'
			}],

			// /* Row Body Table - Indicators */
			[{
				text: 'Aliquam debitis at fuga labore, accusantium molestias dolorem iste recusandae similique ipsa eius dolorum corporis amet perferendis, adipisci blanditiis aspernatur quas incidunt!', 
				colSpan: 5
			},
			{'': ''},  {'':''},  {'':''},  {'':''}
			],
			[{
				text: 'CIENCIAS NATURALES Y EDUCACIÓN AMBIENTAL', 
				colSpan: 4, 
				style: 'tableHeader'
			},
			{'': ''},{'': ''},{'': ''},
			{
				text: 'Definitiva', 
				style: 'tableHeader',
				alignment: 'right', 
			},
			],

			// /* Row SubHeader Table - Subject */
			[{
				text: 'Asignatura: FÍSICA', 
				style: 'subject', 
				colSpan: 3
			}, 
			{'': ''},  {'':''},
			{
				text: 'IH: 3', 
				style: 'subject'
			}, 
			{
				text: '72 DB', 
				style: 'subject', 
				alignment: 'right', 
				bold: true, 
				color: 'green'
			}],

			// /* Row Body Table - Indicators */
			[{
				text: 'Aliquam debitis at fuga labore, accusantium molestias dolorem iste recusandae similique ipsa eius dolorum corporis amet perferendis, adipisci blanditiis aspernatur quas incidunt!', 
				colSpan: 5
			},
			{'': ''},  {'':''},  {'':''},  {'':''}
			]
			]
		}
	},
	[
	{
		margin: [0, 10, 0, 0],
		text: [
		{ text: 'NOTAS: ', bold: true },
		'(DS) Desempeño superior [90-100]; (DA) Desempeño básico (70-79); (Db) Desempeño bajo (10-69)',
		]
	}
	],
	{ text: 'OBSERVACIONES: ', bold: true, margin: [0, 10, 0, 0] },
	{canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
	{canvas: [{ type: 'line', x1: 0, y1: 15, x2: 595-2*40, y2: 15, lineWidth: 1 }]},
	{
		margin: [0, 10, 0, 0],
		alignment: 'center',
		bold: true,
		fontSize: 12,
		text: 'RUZ ROMERO KENNIT DAVID'
	},
	{
		alignment: 'center',
		fontSize: 10,
		text: 'Líder de Curso'
	},
	],
	styles: {
		h1: {
			margin: [0, 10, 0, 0],
			fontSize: 15,
			bold: true
		},
		h2: {
			margin: [0, 5, 0, 0],
			fontSize: 10,
			bold: true
		},
		h3: {
			fontSize: 5,
			bold: 'normal'
		},
		subheader: {
			margin: [0, 5, 0, 5],
		},
		tableStyle: {
			borders: [false, false, false, false]
		},
		tableHeader: {
			fontSize: 10,
			bold: true,
			fillColor: '#DDD',
			color: 'black',
		},
	},
	defaultStyle: {
		fontSize: 8		
	},
}
var pdf = pdfMake.createPdf(doc);
pdf.open();
// pdf.download();