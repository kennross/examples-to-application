var doc = {
	content: [
	{
		columns: [
		{
			text: 'Seguimiento profesor: ' + 'Kennit David Ruz Romero',
			alignment: 'left'
		},
		{
			text: 'Periodo Lectivo ' + 1,
			alignment: 'right'
		}	
		]			
	},


	{
		// style: 'tableStyle',
		// layout: 'lightHorizontalLines', // optional
		table: {
			widths: ['*', 'auto', 'auto', 'auto', 'auto'],
			body: [
			/* Row Header Table - Area */
			[{
				text: 'CIENCIAS NATURALES Y EDUCACIÓN AMBIENTAL', 
				colSpan: 4, 
				style: 'tableHeader'
			},
			{'': ''},{'': ''},{'': ''},
			{
				text: 'Definitiva', 
				style: 'tableHeader',
				alignment: 'right', 
			},
			],

			// /* Row SubHeader Table - Subject */
			[{
				text: 'Asignatura: FÍSICA', 
				style: 'subject', 
				colSpan: 3
			}, 
			{'': ''},  {'':''},
			{
				text: 'IH: 3', 
				style: 'subject'
			}, 
			{
				text: '72 DB', 
				style: 'subject', 
				alignment: 'right', 
				bold: true, 
				color: 'green'
			}],

			// /* Row Body Table - Indicators */
			[{
				text: 'Aliquam debitis at fuga labore, accusantium molestias dolorem iste recusandae similique ipsa eius dolorum corporis amet perferendis, adipisci blanditiis aspernatur quas incidunt!', 
				colSpan: 5
			},
			{'': ''},  {'':''},  {'':''},  {'':''}
			],
			[{
				text: 'CIENCIAS NATURALES Y EDUCACIÓN AMBIENTAL', 
				colSpan: 4, 
				style: 'tableHeader'
			},
			{'': ''},{'': ''},{'': ''},
			{
				text: 'Definitiva', 
				style: 'tableHeader',
				alignment: 'right', 
			},
			],

			// /* Row SubHeader Table - Subject */
			[{
				text: 'Asignatura: FÍSICA', 
				style: 'subject', 
				colSpan: 3
			}, 
			{'': ''},  {'':''},
			{
				text: 'IH: 3', 
				style: 'subject'
			}, 
			{
				text: '72 DB', 
				style: 'subject', 
				alignment: 'right', 
				bold: true, 
				color: 'green'
			}],

			// /* Row Body Table - Indicators */
			[{
				text: 'Aliquam debitis at fuga labore, accusantium molestias dolorem iste recusandae similique ipsa eius dolorum corporis amet perferendis, adipisci blanditiis aspernatur quas incidunt!', 
				colSpan: 5
			},
			{'': ''},  {'':''},  {'':''},  {'':''}
			]
			]
		}
	},



	],
	// pageSize: 'FOLIO',
	pageOrientation: 'landscape'
}

var pdf = pdfMake.createPdf(doc);
pdf.open();
// pdf.download();