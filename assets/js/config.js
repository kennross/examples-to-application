moment.locale("es");

$().ready(function() {
	$('[data-toggle="popover"]').popover();
	$('[data-toggle="tooltip"]').tooltip();

	$("#btn_target_add_petition").tooltip({
		title: 'Reportar un problema',
		placement: 'top'
	});

	/* Only Layout Home */
	$("#resources").tooltip({
		title: 'Ver Materiales',
		placement: 'bottom'
	})
});

/* configure token general */
$.ajaxSetup({
	dataType: 'JSON',
	method: 'POST',
	headers: {
		'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	},
	error: function() {
		swal({
			title: "¡Ups!",
			text: "Ocurrió un error, estamos trabajando en ello, intente más tarde",
			type: "error",
			timer: 3500,
			showConfirmButton: false
		})
	}
});

/* configure toastr position */
toastr.options = {
	"positionClass": "toast-bottom-right"
}

/* Lenguaje Validator */
var myLanguage = {
	errorTitle: '¡Envío del formulario fallido!',
	requiredFields: 'No ha respondido a todos los campos requeridos',
	badTime: 'No ha dado una hora correcta',
	badEmail: 'No ha dado una dirección de correo electrónico correcta',
	badTelephone: 'No ha dado un número de teléfono correcto',
	badSecurityAnswer: 'No ha dado una respuesta correcta a la pregunta de seguridad',
	badDate: 'No ha dado una fecha correcta',
	lengthBadStart: 'El valor de entrada debe estar entre ',
	lengthBadEnd: ' caracteres',
	lengthTooLongStart: 'El valor de entrada es mayor que ',
	lengthTooShortStart: 'El valor de entrada es más corto que ',
	notConfirmed: 'La contraseña no coincide',
	badDomain: 'Ámbito de los valores incorrectos',
	badUrl: 'El valor de entrada no es una URL correcta',
	badCustomVal: 'El valor de entrada es incorrecta',
	andSpaces: ' y espacios ',
	badInt: 'El valor de entrada no es un número correcto',
	badSecurityNumber: 'Su número de la seguridad social era incorrecta',
	badUKVatAnswer: 'Número incorrecto IVA del Reino Unido',
	badStrength: 'La contraseña no es lo suficientemente fuerte',
	badNumberOfSelectedOptionsStart: 'Tienes que elegir al menos ',
	badNumberOfSelectedOptionsEnd: ' respuestas',
	badAlphaNumeric: 'El valor de entrada sólo puede contener caracteres alfanuméricos ',
	badAlphaNumericExtra: ' y ',
	wrongFileSize: 'El archivo que está tratando de subir es demasiado grande (máximo %s)',
	wrongFileType: 'Sólo los archivos de tipo %s están permitidos',
	groupCheckedRangeStart: 'Por favor, elija entre ',
	groupCheckedTooFewStart: 'Por favor, elija al menos ',
	groupCheckedTooManyStart: 'Por favor, elija un máximo de ',
	groupCheckedEnd: ' item(s)',
	badCreditCard: 'El número de tarjeta de crédito no es correcta',
	badCVV: 'El número CVV no es correcto',
	wrongFileDim : 'Dimensiones de imagen incorrectos,',
	imageTooTall : 'la imagen no puede ser más alto que',
	imageTooWide : 'la imagen no puede ser más amplio que',
	imageTooSmall : 'la imagen es demasiado pequeÃ±o',
	min : 'mínimo',
	max : 'máximo',
	imageRatioNotAccepted : 'El radio de la imagen no es aceptada',
	requiredField: 'Este campo es obligatorio'
};

$.validate({
	// language : myLanguage
	lang: 'es'
});

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-74789734-1', 'auto');
ga('send', 'pageview');

/* Vue Filter Predefine */
Vue.filter('date_string', function (value) {
	return  moment(value, "YYYY-MM-DD HH:mm").subtract(5, 'hours').format("MMM D - Y");
});

Vue.filter('from_now', function (value) {
	return moment(value, "YYYY-MM-DD HH:mm").subtract(5, 'hours').fromNow();
});