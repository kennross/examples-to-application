function startAplication(type_app, callback, app, callbackSuccess) {
	$.ajax({
		url: '/create/session',
		data: {
			app: type_app
		},
		type: 'POST',
		success: function(server) {
			app.session = server;
			if (callbackSuccess != null) {
				callbackSuccess();
			}
		},
		complete: function() {
			callback();
		}
	});
}

function startAjaxLoader(btn, text) {
	$('#' + btn).attr('disabled', true);
	$('#' + btn).html('<i class="fa fa-cog fa-spin"></i>&nbsp; ' + text);
}

function finishAjaxLoader(btn, text) {
	$('#' + btn).removeAttr('disabled');
	$('#' + btn).html(text);
}

function readURL(input, box_id, type) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			if (type) {
				$('#' + box_id).attr('src', e.target.result);
			} else {
				$('#' + box_id).attr('style', 'background-image: url(' + e.target.result + ')');
			}
		}
		reader.readAsDataURL(input.files[0]);

		setTimeout(function(){
			console.clear()
		}, 1000);
	}
}

function uploadFile(id, path_storage, success, complete) {
	var data = new FormData();
	data.append('file', document.getElementById(id).files[0]);
	data.append('path', path_storage);

	$.ajax({
		url: '/util/uploadfile',
		enctype: 'multipart/form-data',
		data: data,
		cache: false,
		processData: false,
		contentType: false,
		success: function(name_asset) {
			success(name_asset);
		},
		complete: function() {
			if (complete != null) {
				complete();
			}
		}
	});
}

function resetSelectPicker() {
	Vue.nextTick(function() {
		$('.selectpicker').selectpicker('refresh');
	});
}

Vue.filter('truncate', function (text, stop, clamp) {
	return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
})