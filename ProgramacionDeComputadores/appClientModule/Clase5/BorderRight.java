package Clase5;

public class BorderRight {

	public static void main(String[] args) {
		System.out.println("HISTORY FILA DERECHA");
		
		int[][] history = { { 2, 7 } };

		int x = history[history.length - 1][0];
		int y = history[history.length - 1][1];
		
		int[][] attemp1 = { { x, y }, { 1, 6 } };
		int[][] attemp2 = { { x, y }, { 1, 7 } };
		int[][] attemp3 = { { x, y }, { 1, 8 } };
		
		System.out.println("attemp1: " + allowMoveBorder(attemp1) + "\n");
		System.out.println("attemp2: " + allowMoveBorder(attemp2) + "\n");
		System.out.println("attemp3: " + allowMoveBorder(attemp3) + "\n");
		
		
		System.out.println("-- Other Test --\n");
		
		int[][] history_no = { { 1, 7 } };

		int xno = history_no[history_no.length - 1][0];
		int yno = history_no[history_no.length - 1][1];
		
		int[][] nattemp1 = { { xno, yno }, { 1, 6 } };
		int[][] nattemp2 = { { xno, yno }, { 1, 8 } };
		
		System.out.println("nattemp1: " + allowMoveBorder(nattemp1) + "\n");
		System.out.println("nattemp2: " + allowMoveBorder(nattemp2) + "\n");
	}

	public static boolean allowMoveBorder(int[][] attemp) {
		System.out.println("Actual  F: (" + attemp[0][1] + ")");
		System.out.println("Intento F: (" + attemp[1][1] + ")");

		// FILA Actual: attemp[0][1]
		// FILA No Permitida: 12 y 2

		// COLUMNA Actual: attemp[0][0]
		// COLUMNA No Permitida: 1 y 9
		if ((attemp[0][1] != 12 && attemp[0][1] != 2) && (attemp[0][0] != 9 && attemp[0][0] != 1)) {
			return true;
		} else {
			return false;
		}
	}

}
