package Clase5;

import java.util.Random;

public class Trayectoria {

	public static void main(String[] args) {
		int[][] history = { { 5, 7 }, { 5, 8 }, { 4, 7 }, { 4, 8 }, { 3, 8 }, { 4, 9 }, { 5, 9 }, { 5, 10 }, { 6, 10 },
				{ 5, 11 }, { 6, 11 }, { 5, 12 }, };

		int x = history[history.length - 1][0];
		int y = history[history.length - 1][1];

		int[] initAttemp = moveRandom(1, true);

		int[][] attemp = { { x, y }, { x + initAttemp[0], y + initAttemp[1] } };

		int count = 0;

		while (posibleTrajectory(history, attemp) && count < 20) {
			int[] otherAttemp = moveRandom(1, false);

			attemp[1][0] = attemp[0][0] + otherAttemp[0];
			attemp[1][1] = attemp[0][1] + otherAttemp[1];

			count++;
		}

		System.out.println("\nIntento FINAL: " + attemp[1][0] + " " + attemp[1][1]);

	}

	public static boolean posibleTrajectory(int[][] history, int[][] attemp) {
		System.out.println("Actual y Futuro: (" + attemp[0][0] + ", " + attemp[0][1] + ") - (" + attemp[1][0] + ", "
				+ attemp[1][1] + ")");
		for (int i = 0; i < history.length - 1; i++) {

			if (((attemp[0][0] == history[i][0] && attemp[0][1] == history[i][1])
					&& (attemp[1][0] == history[i + 1][0] && attemp[1][1] == history[i + 1][1]))
					|| ((history[i + 1][0] == attemp[0][0] && history[i + 1][1] == attemp[0][1])
							&& (history[i][0] == attemp[1][0] && history[i][1] == attemp[1][1])))

				return true;

		}

		return !allowMoveBorder(attemp);
	}

	public static boolean allowMoveBorder(int[][] attemp) {
		if (attemp[1][0] > 1 && attemp[1][0] < 9 && attemp[1][1] > 2 && attemp[1][1] < 12) {
			return true;
		} else {
			return false;
		}
	}

	public static int[] moveRandom(int inclination, boolean withStrategy) {
		Random gen = new Random();
		if (withStrategy) {
			if (inclination < 1) {
				int[][] possibilitiesOne = { { -1, 0 }, { +1, 0 }, { +1, +1 }, { 0, +1 }, { -1, +1 } };
				return possibilitiesOne[gen.nextInt(5)];
			} else {
				int[][] possibilitiesZero = { { -1, 0 }, { +1, 0 }, { -1, -1 }, { 0, -1 }, { +1, -1 } };
				return possibilitiesZero[gen.nextInt(5)];
			}
		} else {
			int[][] possibilities = { { -1, 0 }, { +1, 0 }, { +1, +1 }, { -1, -1 }, { 0, +1 }, { 0, -1 }, { -1, +1 },
					{ +1, -1 } };
			return possibilities[gen.nextInt(8)];
		}
	}
}
