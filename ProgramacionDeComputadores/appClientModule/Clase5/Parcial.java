package Clase5;


public class Parcial {

	public static void main(String[] args) {

		System.out.println("* - - PARCIAL SEGUNDO CORTE - - * \n");
		
		
		System.out.println("EJERCICIO NÚMERO 2");

		char matrizPrueba[][] = { 
				{ 'z', 'z', 'y' }, 
				{ 'z', 'z', 'p' }, 
				{ 'z', 'z', 't' }, 
				{ 'z', 'z', 'ñ' },
				{ 'z', 'z', 'l' }, 
				{ 'z', 'z', 'q' }, };
		
		char[] arregloA = {'z', 'z', 'z', 'z', 'z', 'z'};
		
		char[] arregloB = {'g', 'u', 'l', 'i', 'b', 'o'};
		
		char[] arregloC = {'u', 'o', 'm', 'x'};
		
		System.out.println("\nMatriz");
		for (int i = 0; i < matrizPrueba.length; i++) {
			for (int j = 0; j < matrizPrueba[i].length; j++) {
				System.out.print(matrizPrueba[i][j] + " ");
			}
			System.out.println("");
		}
		
		System.out.println("\nArreglo A: ");
		for (int i = 0; i < arregloA.length; i++) {
			System.out.print(arregloA[i] + " ");
		}
		
		System.out.println("\n\nArreglo B: ");
		for (int i = 0; i < arregloB.length; i++) {
			System.out.print(arregloB[i] + " ");
		}
		
		System.out.println("\n\nArreglo C: ");
		for (int i = 0; i < arregloC.length; i++) {
			System.out.print(arregloC[i] + " ");
		}
		
		
		System.out.println("\n\n- - - SEPARACIÓN PARA PRUEBAS - - -");
		
		System.out.println("\nLa matriz con arregloA (TRUE): " + (inMatriz(matrizPrueba, arregloA) ? "Es igual a alguna columna" : "No es igual a ninguna columna"));	
		System.out.println("La matriz con arregloB (FALSE): " + (inMatriz(matrizPrueba, arregloB) ? "Es igual a alguna columna" : "No es igual a ninguna columna"));
		System.out.println("La matriz con arregloC (FALSE): " + (inMatriz(matrizPrueba, arregloC) ? "Es igual a alguna columna" : "No es igual a ninguna columna"));
		
		System.out.println("\n\n- - - SEPARACIÓN EJERCICIO - - -\n\n");
		
		System.out.println("EJERCICIO NÚMERO 1 \n");
		
		int[] amigo1 = {1, 0, 2, 2, 4, 2, 2, 1};
		int[] amigo2 = {2, 0, 1, 3, 1, 0, 2, 1};
		int[] amigo3 = {1, 1, 0, 2, 1, 2, 2, 2};
		int[] amigo4 = {0, 0, 1, 2, 1, 0, 0, 1};
		
		
		int[] salidaFacilyDificilAmigos4 = laminasFacilDificil(amigo1, amigo2, amigo3, amigo4);
		
		System.out.println("AMIGOS (1-4): Lamina fácil: " + salidaFacilyDificilAmigos4[0] + " y lámina difícil: " + salidaFacilyDificilAmigos4[1]);
		
		int[] amigo5 = {1, 0, 2, 2, 1};
		int[] amigo6 = {2, 3, 1, 3, 0, 2, 1};
		int[] amigo7 = {1, 1, 0, 2, 1, 2, 2, 2};
		int[] amigo8 = {2, 1, 1, 2, 1, 0, 1};
		
		
		int[] salidaFacilyDificil8 = laminasFacilDificil(amigo5, amigo6, amigo7, amigo8);
		
		System.out.println("AMIGOS (5-8): Lamina fácil: " + salidaFacilyDificil8[0] + " y lámina difícil: " + salidaFacilyDificil8[1]);
		
		
		int[] amigo9 = {};
		int[] amigo10 = {};
		int[] amigo11 = {};
		int[] amigo12 = {};
		
		
		int[] salidaFacilyDificil12 = laminasFacilDificil(amigo9, amigo10, amigo11, amigo12);
		
		System.out.println("AMIGOS (9-12): Lamina fácil: " + salidaFacilyDificil12[0] + " y lámina difícil: " + salidaFacilyDificil12[1]);
		
		int[] amigo13 = {1, 0, 0, 2, 4, 2, 2, 1, 1, 9};
		int[] amigo14 = {2, 9, 1, 3, 1, 0, 2, 1, 7, 19};
		int[] amigo15 = {1, 1, 0, 2, 1, 2, 2, 2, 3, 6};
		int[] amigo16 = {0, 0, 1, 2, 1, 0, 0, 1, 7, 3};
		
		
		int[] salidaFacilyDificilAmigos16 = laminasFacilDificil(amigo13, amigo14, amigo15, amigo16);
		
		System.out.println("AMIGOS (13-16): Lamina fácil: " + salidaFacilyDificilAmigos16[0] + " y lámina difícil: " + salidaFacilyDificilAmigos16[1]);
	}
	
	
	public static int[] laminasFacilDificil(int[] a, int[] b, int[] c, int[] d) {
		int[] dyF = new int[2];
		
		if ((a.length == b.length && b.length == c.length && c.length == d.length) && ((a.length > 0 && b.length > 0 && c.length > 0 && d.length > 0))) {
			int[] laminas = new int[a.length];
			
			for (int i = 0; i < a.length; i++) {
				laminas[i] += a[i];
				laminas[i] += b[i];
				laminas[i] += c[i];
				laminas[i] += d[i];
			}
			
			dyF[0] = determinarPosicionMayorMenor(laminas, true);
			dyF[1] = determinarPosicionMayorMenor(laminas, false);
		} else {
			dyF[0] = -1;
			dyF[1] = -1;
		}
		
		return dyF;
		
	}
	
	public static int determinarPosicionMayorMenor(int[] laminas, boolean lado) {
		int indice = 0;
		
		int actual = laminas[0];
		for (int i = 1; i < laminas.length; i++) {
			
			if (lado) {
				if (laminas[i] > actual) {
					actual = laminas[i];
					indice = i;
				}
			} else {
				if (laminas[i] < actual) {
					actual = laminas[i];
					indice = i;
				}	
			}
		}
		
		return indice;
	}
	

	/*
	 * El algoritmo hace lo siguiente para solucionar el problema:
	 * - Recorre la matriz en términos de columna
	 * - Compara cada posición de la columna con la posición del arreglo
	 * - Va contando cuando éstas comparaciones son ciertas
	 * - Si el contador es igual al tamaño del arreglo, es porque coincidió en todas las posiciones
	 */
	public static boolean inMatriz(char[][] matriz, char[] arreglo) {
		// En caso de que la matriz en términos de columna sea diferente al arreglo de comparación, no se hará nada
		if (matriz.length != arreglo.length) {
			return false;
		} else {
			int sum = 0;
			
			for (int i = 0; i < matriz[0].length; i++) {
				sum = 0;
				for (int j = 0; j < matriz.length; j++) {					
					if (matriz[j][i] == arreglo[j]) {
						sum++;
					}		 
				}
				if (sum == arreglo.length) {
					return true;
				}
			}
			return false;	
		}
	}

}
