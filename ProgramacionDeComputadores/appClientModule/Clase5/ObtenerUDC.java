package Clase5;

import java.util.Scanner;

public class ObtenerUDC {

	public static void main(String[] args) {
		
		int generalNum = 5873;
		
		System.out.println("Unidades: " + ObtenerUDC.obtenerUnidades(generalNum));
		System.out.println("Decenas: " + ObtenerUDC.obtenerDecenas(generalNum));
		System.out.println("Centenas: " + ObtenerUDC.obtenerCentenas(generalNum));
	}
	
	static double obtenerUnidades(int num) {
		return num % 10;
	}
	
	static double obtenerDecenas(int num) {
		return num / 10 % 10;
	}
	
	static double obtenerCentenas(int num) {
		return num / 100 % 10;
	}
}
