package Clase5;

import java.util.Scanner;

public class Arreglos {

	public static void main(String[] args) {
		// Conceptos
		/*
		 * En un conjunto de datos de l ismo tipo almacenados de forma contigua (uno al
		 * lado de otro)
		 * 
		 * Puedo hacer 5 operaciones - 1) Declarar (double nota; puedo guardar solo una
		 * nota) (double notas[];
		 * 
		 * - 2) Crear notas = new double[5];
		 * 
		 * - 3) Inicializar (lo hace automáticamente Java) Los enteros inicializan en 0
		 * Los reales inicializan en 0.0 Los booleanos en false Los chars en '\00000'
		 * String inicializa en null
		 * 
		 * Inicialización directa con llaves double - double notasDirectas[] = {3.3, 3,
		 * 4.3, 1.8, 3.7};
		 * 
		 * 4) Acceso - notas[2]
		 * 
		 * 5) Recorrido notas.length y un for desde 0
		 */

		double notas[] = new double[5];
		int notasInts[] = new int[5];
		boolean notasBooleans[] = new boolean[5];
		char notasChats[] = new char[5];

		double notasDirectas[] = { 3.3, 3, 4.3, 1.8, 3.7 };
		/*
		 * for (int i = 0; i < notas.length; i++) { System.out.println("Double: " +
		 * notas[i]); System.out.println("DoubleDirecto: " + notasDirectas[i]);
		 * System.out.println("Int: " + notasInts[i]); System.out.println("Boolean: " +
		 * notasBooleans[i]); System.out.println("Char: " + notasChats[i]); }
		 */

		/*
		 * // Leer varios números int tam;
		 * 
		 * Scanner in = new Scanner(System.in);
		 * System.out.print("Porfavor, el tamaño: "); tam = in.nextInt(); int numeros[]
		 * = new int[tam];
		 * 
		 * for (int i = 0; i < numeros.length; i++) {
		 * System.out.print("Insertar número " + (i + 1) + ": "); numeros[i] =
		 * in.nextInt(); }
		 * 
		 * for (int i = 0; i < numeros.length; i++) { System.out.println(numeros[i]); }
		 */

		int misNotas[] = { 4, 3, 5, 2, 1 };

		System.out.println("Su promedio es {4, 3, 5}: " + promedio(misNotas));
		System.out.println("El mayor de {4, 3, 5} [5]: " + mayorDe(misNotas));

		char arregloDeChars[] = { 'A', 'B', 'C', 'D', 'E', 'F' };
		System.out.println("Arreglo de Char (a)");
		char arregloFinalA[] = devolverSegunA(arregloDeChars);
		for (int i = 0; i < arregloFinalA.length; i++) {
			System.out.println(arregloFinalA[i]);
		}

		System.out.println("Arreglo de Char (B)");
		char arregloFinalB[] = devolverSegunB(arregloDeChars);
		for (int i = 0; i < arregloFinalB.length; i++) {
			System.out.println(arregloFinalB[i]);
		}

		System.out.println("Imprimiendo arreglo con parámetros 5 y 1,2,3,4,5,6,7,8,10");
		short arregloDeShorts[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		short finalArregloSinIndex[] = devolverSinIndex(0, arregloDeShorts);
		for (int i = 0; i < finalArregloSinIndex.length; i++) {
			System.out.println(finalArregloSinIndex[i]);
		}
		
		byte numerosOrdenados[] = {1,2,3};
		System.out.println("Numeros Ordenados? {5,2,3,4,5,6,7,8}: " + ordenadoAscendente(numerosOrdenados));
		
		byte numerosNoOrdenados[] = {1,3,2,4,5,6,7,8};
		System.out.println("Numeros Ordenados? {1,3,2,4,5,6,7,8}: " + ordenadoAscendente(numerosNoOrdenados));
		
		boolean arregloDeBooleans[] = {};
		System.out.println("Está vacío: " + estaVacio(arregloDeBooleans));
		
	}

	public static double promedio(int[] notas) {
		double suma = 0;

		for (int i = 0; i < notas.length; i++) {
			suma += notas[i];
		}

		return suma / notas.length;
	}

	public static int mayorDe(int[] notas) {
		int mayorActual = notas[0];
		for (int i = 0; i < notas.length; i++) {
			if (notas[i] > mayorActual) {
				mayorActual = notas[i];
			}
		}

		return mayorActual;
	}

	public static char[] devolverSegunA(char[] arregloChar) {
		char finalArreglo[] = new char[arregloChar.length - 1];

		for (int i = 0; i < arregloChar.length - 1; i++) {
			finalArreglo[i] = arregloChar[i];
		}

		return finalArreglo;
	}

	public static char[] devolverSegunB(char[] arregloChar) {
		char finalArreglo[] = new char[arregloChar.length - 1];

		for (int i = 0; i < arregloChar.length - 1; i++) {
			finalArreglo[i] = arregloChar[i + 1];
		}

		return finalArreglo;
	}

	public static short[] devolverSinIndex(int n, short[] numeros) {
		if (n > numeros.length - 1) {
			return numeros;
		} else {
			short[] miFinalArreglo = new short[numeros.length - 1];
			
			for (int i = 0; i < n; i++) {
				miFinalArreglo[i] = numeros[i];
			}
			
			for (int i = n + 1; i < numeros.length; i++) {
				miFinalArreglo[i - 1] = numeros[i];
			}
			
			return miFinalArreglo;
		}
	}
	
	public static boolean ordenadoAscendente(byte[] numeros) {
		boolean ordenado = true;
		int actual = numeros[0];
		for (int i = 1; i < numeros.length; i++) {
			if (actual > numeros[i]) {
				ordenado = false;
			}
			
			actual = numeros[i];
		}
		
		return ordenado;
	}
	
	public static boolean estaVacio(boolean[] arregloBoolean) {
		if (arregloBoolean.length == 0) {
			return true;
		} else {
			return false;
		}
	}
}
