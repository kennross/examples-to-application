package Clase5;

public class EJ_HRLC2 {

	public static void main(String[] args) {
		// Sólo funciona cuando las matrices son cuadradas

		int matriz[][] = { { 3, 5, 7 }, { 8, 9, 2 }, { 1, 0, 6 } };

		System.out.println("Matrix Original Proof 1");

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}

		int resultado[][] = capturar(matriz);

		System.out.println("Matrix of HR1 y LC2 (proof 1)");

		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[i].length; j++) {
				System.out.print(resultado[i][j] + " ");
			}
			System.out.println("");
		}

		System.out.println("\n--- Separación de pruebas ---\n");

		int matriz2[][] = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 0, 9, 8 }, { 7, 6, 5, 4 } };

		System.out.println("Matriz Original Proof 2");

		for (int i = 0; i < matriz2.length; i++) {
			for (int j = 0; j < matriz2[i].length; j++) {
				System.out.print(matriz2[i][j] + " ");
			}
			System.out.println("");
		}

		int resultado2[][] = capturar(matriz2);

		System.out.println("Matrix of HR1 y LC2 (proof 2)");

		for (int i = 0; i < resultado2.length; i++) {
			for (int j = 0; j < resultado2[i].length; j++) {
				System.out.print(resultado2[i][j] + " ");
			}
			System.out.println("");
		}
	}

	public static int[][] capturar(int[][] entrada) {
		int salida[][] = new int[2][entrada.length];

		for (int i = 0; i < entrada.length; i++) {

			int mayorActual = entrada[i][0];
			int menorActual = entrada[0][i];

			for (int j = 0; j < entrada[i].length; j++) {
				if (entrada[i][j] > mayorActual) {
					mayorActual = entrada[i][j];
				}

				if (entrada[j][i] < menorActual) {
					menorActual = entrada[j][i];
				}
			}

			salida[0][i] = mayorActual;
			salida[1][i] = menorActual;
		}

		System.out.println("");

		return salida;
	}

}
