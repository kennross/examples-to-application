package Clase5;

public class EjerciciosArboles {

	public static void main(String[] args) {
		// Kennit David Ruz Romero
		// Maria Paula Moscoso
		// Ana Maria Tobacía

		/*
		 * Aplicando UN TABLERO:
		 */
		System.out.println("PUNTO 5");
		System.out.println("Puntaje para (6,6): " + calcularPuntaje(6, 6));
		System.out.println("Puntaje para (-0.5,-0.5): " + calcularPuntaje(-0.5, -0.5));
		System.out.println("Puntaje para (2.5,2.5): " + calcularPuntaje(2.5, 2.5));

		System.out.println("- - - - - - SEPARACIÓN - - - - - - -");

		System.out.println("PUNTO 6");
		System.out.println("Desciende (DOS Descienden): " + calcularDescendencia(10, 10, 5, 3, 5, 3, 3, 3, 17, 17));

		System.out.println("Desciende (Municipal POR P): " + calcularDescendencia(10, 9, 5, 3, 5, 3, 3, 3, 17, 17));
		System.out.println("Desciende (Tigres POR P): " + calcularDescendencia(9, 10, 5, 3, 5, 3, 3, 3, 17, 17));

		System.out.println(
				"Desciende (Municipal POR Diferencia): " + calcularDescendencia(10, 10, 6, 3, 5, 3, 3, 3, 17, 17));
		System.out.println(
				"Desciende (Tigres POR Diferencia): " + calcularDescendencia(10, 10, 5, 3, 6, 3, 3, 3, 17, 17));

		System.out.println("Desciende (Municipal POR Partidos Ganados): "
				+ calcularDescendencia(10, 10, 5, 3, 5, 3, 8, 4, 17, 17));
		System.out.println(
				"Desciende (Tigres POR Partidos Ganados): " + calcularDescendencia(10, 10, 5, 3, 5, 3, 4, 8, 17, 17));

		System.out.println("Desciende (Municipal POR Partidos Ganados): "
				+ calcularDescendencia(10, 10, 5, 3, 5, 3, 8, 8, 26, 17));
		System.out.println(
				"Desciende (Tigres POR Partidos Ganados): " + calcularDescendencia(10, 10, 5, 3, 5, 3, 8, 8, 12, 17));

		System.out.println("- - - - - - SEPARACIÓN - - - - - - -");
		System.out.println("PUNTO 10");

		/*
		 * c. una función main que solicite al usuario los valores de h1, m1, s1, h2, m2
		 * y s2, e imprima el nombre del ganador seguido de la cantidad de segundos de
		 * diferencia con que obtuvo el triunfo, o la palabra “Empate” si es el caso.
		 */
		String resultadoCompetencia = definirGanador(10, 3, 50, 37, 15, 23);
		int diferenciaCompetencia = definirDiferenciaGanadorPerdedor(10, 3, 50, 37, 15, 23);

		System.out.print("Juan (10h, 3m, 50s) y David (37h, 15m, 23s: " + resultadoCompetencia);
		if (!resultadoCompetencia.equals("Empate")) {
			System.out.println("y la diferencia es de: " + diferenciaCompetencia + "seg");
		}
		
		resultadoCompetencia = definirGanador(10, 3, 50, 10, 3, 49);
		diferenciaCompetencia = definirDiferenciaGanadorPerdedor(10, 3, 50, 10, 3, 49);
		
		System.out.print("Juan (10h, 3m, 50s) y David (10h, 3m, 49s: " + resultadoCompetencia);
		if (!resultadoCompetencia.equals("Empate")) {
			System.out.println("y la diferencia es de: " + diferenciaCompetencia + "seg");
		}
		
		System.out.println("- - - - - - SEPARACIÓN - - - - - - -");
		System.out.println("PUNTO 11");
		System.out.println("Uno movimiento: " + calcularNumeroDeMovimientos(1, 1, 7, 7));
		System.out.println("Dos movimientos: " + calcularNumeroDeMovimientos(3, 7, 6, 2));
		System.out.println("Dos movimientos: " + calcularNumeroDeMovimientos(1, 1, 1, 1));
		System.out.println("Dos movimientos: " + calcularNumeroDeMovimientos(3, 3, 6, 7));
		

	}

	/*
	 * Un tablero de dardos está compuesto por cinco aros concéntricos como se
	 * muestra en la figura. La tabla siguiente muestra el radio de cada uno de
	 * estos aros y el puntaje otorgado por un dardo en cada uno de los colores.
	 * 
	 * Suponga que el centro del tablero es el punto (0, 0). Calcule el puntaje
	 * obtenido con un dardo que golpea el tablero en el punto (x, y).
	 */
	static byte calcularPuntaje(double x, double y) {
		byte puntaje;

		double radio = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

		if (radio <= 1) {
			puntaje = 15;
		} else if (radio > 1 && radio <= 2) {
			puntaje = 9;
		} else if (radio > 2 && radio <= 3) {
			puntaje = 5;
		} else if (radio > 3 && radio <= 4) {
			puntaje = 2;
		} else if (radio > 4 && radio <= 5) {
			puntaje = 1;
		} else {
			puntaje = 0;
		}

		return puntaje;
	}

	/*
	 * La Liga Halcón ha llegado a su fin y dos equipos, Tigres y Municipal, han
	 * quedado en los dos últimos lugares. Para decidir cuál desciende a la B, se
	 * siguen las reglas presentadas a continuación:
	 * 
	 * - Desciende el equipo con menor número de puntos en la liga. (Pa, Pb)
	 * 
	 * - Si hay empate en puntos, desciende el equipo con la menor diferencia de
	 * goles a favor y goles en contra. (GAa, GAb) (GEa, GEb)
	 * 
	 * - Si hay empate en puntos y diferencia de goles, desciende el equipo con
	 * menor cantidad de goles a favor.
	 * 
	 * - Si hay empate en puntos, diferencia de goles y goles a favor, desciende el
	 * equipo con menor cantidad de partidos ganados. (CPGa, CPGb)
	 * 
	 * - Si hay empate en puntos, diferencia de goles, goles a favor y cantidad de
	 * partidos ganados, desciende el equipo con menor puntaje de juego limpio.
	 * (PJLa, PJLb)
	 * 
	 * - Si hay empate en puntos, diferencia de goles, goles a favor, partidos
	 * ganados (los dos descienden) y juego limpio, los dos descienden.
	 */

	static String calcularDescendencia(int Pa, int Pb, int GAa, int GEa, int GAb, int GEb, int CPGa, int CPGb, int PJLa,
			int PJLb) {

		/*
		 * a := Tigres b := Municipal
		 */

		if (Pa > Pb) {
			return "Municipal";
		} else if (Pa < Pb) {
			return "Tigres";
		} else if ((GAa - GEa) > (GAb - GEb)) {
			return "Municipal";
		} else if ((GAa - GEa) < (GAb - GEb)) {
			return "Tigres";
		} else if (GAa > GAb) {
			return "Municipal";
		} else if (GAa < GAb) {
			return "Tigres";
		} else if (CPGa > CPGb) {
			return "Municipal";
		} else if (CPGa < CPGb) {
			return "Tigres";
		} else if (PJLa > PJLb) {
			return "Municipal";
		} else if (PJLa < PJLb) {
			return "Tigres";
		} else {
			return "Los dos descienden a la B";
		}

	}

	/*
	 * Juan y David empezaron a correr una maratón al mediodía. Juan terminó a las
	 * h1 horas, m1 minutos y s1 segundos, mientras que David lo hizo a las h2
	 * horas, m2 minutos y s2 segundos.
	 * 
	 * Escriba un programa java que contenga:
	 * 
	 * a. una función java que, a partir de los valores h1, m1, s1, h2, m2 y s2,
	 * determine el resultado de la carrera. La función debe retornar una cadena
	 * conteniendo el nombre del ganador o la palabra “Empate”, de acuerdo al
	 * resultado de la carrera.
	 */
	static String definirGanador(int h1, int m1, int s1, int h2, int m2, int s2) {

		int totalSegundosJuan = (h1 * 3600) + (m1 * 60) + s1;
		int totalSegundosDavid = (h2 * 3600) + (m2 * 60) + s2;

		if (totalSegundosJuan == totalSegundosDavid) {
			return "Empate";
		} else if (totalSegundosJuan < totalSegundosDavid) {
			return "Juan llega primero";
		} else {
			return "David llega primero";
		}
	}

	/*
	 * b. una función que, a partir de los valores h1, m1, s1, h2, m2 y s2, calcule
	 * la cantidad de segundos transcurridos desde la llegada del ganador hasta la
	 * llegada del segundo.
	 */

	static int definirDiferenciaGanadorPerdedor(int h1, int m1, int s1, int h2, int m2, int s2) {
		int totalSegundosJuan = (h1 * 3600) + (m1 * 60) + s1;
		int totalSegundosDavid = (h2 * 3600) + (m2 * 60) + s2;

		int diferencia = totalSegundosJuan - totalSegundosDavid;

		if (diferencia > 0) {
			return diferencia;
		} else {
			return (diferencia * (-1));
		}
	}

	/*
	 * Juan está aprendiendo a jugar Ajedrez. El día de hoy, él está estudiando el
	 * alfil, para lo cual ha creado el siguiente ejercicio:
	 * 
	 * - Ubica un alfil en la posición (f1, c1). 
	 * 
	 * - Selecciona una segunda posición (f2, c2). 
	 * 
	 * - Mueve el alfil a la posición (f2, c2), buscando usar la menor 
	 *   cantidad posible de movimientos.
	 * 
	 * Juan practica muchas veces, pero en algunas ocasiones no está seguro de estar
	 * usando la menor cantidad de movimientos posible.
	 * 
	 * Escriba un programa que reciba los valores f1, c1, f2 y c2, y calcule la
	 * menor cantidad de movimientos que necesita un alfil para moverse de una
	 * posición a la otra.
	 */
	
	static int calcularNumeroDeMovimientos(int f1, int c1, int f2, int c2) {
		if (f1 == f2 && c1 == c2) {
			return 0;
		} else if (f1 == c1 && f2 == c2) {
			return 1;
		} else if ((f1 != c1 && f2 == c2 ) || (f1 == c1 && f2 != c2)) {
			return -1;
		} else {
			return 2;
		}
	}
}
