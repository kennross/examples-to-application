package Clase5;

public class CodigoAna {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] matriz = { { 1, 5, 3, 7 }, { 3, 6, 2, 4 }, { 5, 8, 9, 6 }, { 4, 7, 9, 2 } };

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}

		int sumafl;
		sumafl = sumafila(matriz, 1);
		System.out.println(sumafl);

		int sumacl;
		sumacl = sumacolumna(matriz, 1);
		System.out.println(sumacl);
		
		System.out.println("");
		
		int sumadiagonal;
		sumadiagonal = sumadgprincipal(matriz);
		System.out.println("La suma de la diagonal: " + sumadiagonal);

		/*
		 * int[] sumadgprc; sumadgprc =sumadgprincipal(matriz);
		 * System.out.println(sumadgprc + " ");
		 */

		int[] primerafl;
		primerafl = primerafila(matriz);
		for (int i = 0; i < primerafl.length; i++) {
			System.out.print(primerafl[i] + " ");
		}
		System.out.println(" ");

		int[] ultimafl;
		ultimafl = ultimafila(matriz);
		for (int i = 0; i < ultimafl.length; i++) {
			System.out.print(ultimafl[i] + " ");
		}

		System.out.println(" ");

		int[] primeracl;
		primeracl = primeracolumna(matriz);
		for (int i = 0; i < primeracl.length; i++) {
			System.out.print(primeracl[i] + " ");
		}

		System.out.println(" ");

		int[] ultimacl;
		ultimacl = ultimacolumna(matriz);
		for (int i = 0; i < ultimacl.length; i++) {
			System.out.print(ultimacl[i] + " ");
		}

		System.out.println(" ");

		int[] dgprincipal;
		dgprincipal = diagonalprincipal(matriz);
		for (int i = 0; i < dgprincipal.length; i++) {
			System.out.print(dgprincipal[i] + " ");
		}
		
		System.out.println("");

		int[] dgSegundaria;
		dgSegundaria = diagonalsegundaria(matriz);
		for (int i = 0; i < dgSegundaria.length; i++) {
			System.out.print(dgSegundaria[i] + " ");
		}

	}

	// SUMAR UNA FILA//
	static int sumafila(int matriz[][], int fila) {
		int suma = 0;
		for (int j = 0; j < matriz.length; j++) {
			suma += matriz[fila][j];
		}
		return suma;
	}

	// SUMAR UNA COLUMNA//
	static int sumacolumna(int matriz[][], int columna) {
		int suma = 0;
		for (int i = 0; i < matriz.length; i++) {
			suma += matriz[i][columna];
		}
		return suma;
	}

	// SUMAR LA DIAGONAL PRINCIPAL//
	static int sumadgprincipal(int matriz[][]) {
		int suma = 0;
		for (int i = 0, j = 0; i < matriz.length; i++, j++) {
			suma = suma + matriz[i][j];
		}
		return suma;
	}

	// SUMAR LA DIAGONAL INVERSA//
	static int sumadginversa(int[][] matriz) {
		int suma = 0;
		for (int i = 0, j = 0; i < matriz.length; i++, j--) {
			suma += matriz[i][j];
		}
		return suma;
	}

	// MOSTRAR SOLO LA PIMERA FILA//
	static int[] primerafila(int matriz[][]) {
		return matriz[0];
	}

	// MOSTRAR SOLO LA ULTIMA FILA//
	static int[] ultimafila(int matriz[][]) {
		return matriz[matriz.length - 1];
	}

	// MOSTRAR SOLO LA PRIMERA COLUMNA//
	static int[] primeracolumna(int matriz[][]) {
		int[] pc = new int[matriz.length];
		for (int i = 0; i < pc.length; i++) {
			pc[i] = matriz[i][0];
		}
		return pc;
	}

	// MOSTRAR SOLO LA ULTIMA COLUMNA//
	static int[] ultimacolumna(int matriz[][]) {
		int[] pc = new int[matriz.length];
		for (int i = 0; i < pc.length; i++) {
			pc[i] = matriz[i][matriz.length - 1];
		}
		return pc;
	}

	// MOSTRAR DIAGONAL PRINCIPAL//
	static int[] diagonalprincipal(int matriz[][]) {
		int[] pc = new int[matriz.length];
		for (int i = 0; i < pc.length; i++) {
			pc[i] = matriz[i][i];
		}
		return pc;
	}

	// MOSTRAR DIAGONAL segundaria//
	static int[] diagonalsegundaria(int matriz[][]) {
		int[] pc = new int[matriz.length];
		int j = pc.length - 1;
		for (int i = 0; i < pc.length; i++) {
			pc[i] = matriz[i][j];
			j--;
		}
		return pc;
	}

}
