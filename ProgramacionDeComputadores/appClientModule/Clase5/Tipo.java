package Clase5;

public class Tipo {

	public static void main(String[] args) {
		/* Ejemplos PRESENTACIÓN: TIPOS DE DATOS, CONVERSIÓN IMPLÍCITA Y CASTING. */

		/*
		 * Declaración e inicialización de una cadena de caracteres
		 */
		String nombre = "Kennit David Ruz Romero";

		String parrafoLatin = "Lorem ipsum dolor sit amet," 
				+ "tempor incididunt ut labore et dolore\n"
				+ "quis nostrud exercitation ullamco" 
				+ "consequat. Duis aute irure dolor"
				+ "cillum dolore eu fugiat nulla pariatur.";

		String saludo = "Hola, Bienvenido: " + nombre;

		/*
		 * Declaración e inicialización de una variable de tipo lógico (booleanda)
		 */

		boolean isAdmin = true;
		boolean descuento = false;

		/*
		 * Declaración e inicialización 
		 * de una variable de tipo char (carácter)
		 * Debe ir entre comillas sencillas
		 */

		char inicialNombre = 'K';
		char simboloInfinito = '∞';

		/*
		 * Declaración e inicialización de una variable de tipo byte (entero)
		 */

		byte edad = 20;
		byte temperaturaMinima = -70;

		/*
		 * Declaración e inicialización de una variable de tipo short (entero)
		 */

		short kilometrosCaminadosAnual = 32000;
		short balanceFinal = -29300;

		/*
		 * Declaración e inicialización de una variable de tipo int (entero)
		 */

		int montoMaximoPorTransaccion = 2100000;
		int balanceEnDolares = -1500000;

		/*
		 * Declaración e inicialización de una variable de tipo long (entero)
		 */

		long numeroDeHabitantesLatinoAmerica = 735000000;
		long balanceCredito = -725005400;

		/*
		 * Declaración e inicialización de una variable de tipo float (decimal)
		 */ //
		float promedioMantenerBeca = 3.8f;
		float valorPI = 3.14159265359f;
		float momentoMagnetico = -1.00115965218111f;

		/*
		 * Declaración e inicialización de una variable de tipo double (decimal)
		 */

		double dineroPreciso = 735000000745676543456568.78;
		double interesesMora = 650000000000.83635845;

		/* GENERAL */
		/*
		 * Un casting es una operación especial que nos permite realizar una conversión
		 * entre tipos
		 */

		/* CASTING */

		/*
		 * En el casting explícito sí es necesario escribir código. Ocurre cuando se
		 * realiza una conversión estrecha (narrowing casting), es decir, cuando se
		 * coloca un valor grande en un contenedor pequeño. Son susceptibles de pérdida
		 * de datos.
		 * 
		 * El casting es un procedimiento para transformar una variable primitiva de un
		 * tipo a otro.
		 * 
		 * Existen dos tipos de errores al hacer CASTING - Lo está haciendo mal: Type
		 * mismatch: cannot convert from (dato1) to (dato2) - No se puede hacer dicha
		 * conversión : Cannot cast from (dato1) to (dato2) - No es posible realizar
		 * casting entre una variable primitiva booleana y cualquier otra variable
		 * primitiva. - Sí es posible realizar casting entre una variable primitiva char
		 * y una variable primitiva que almacene enteros
		 * 
		 * Como se ve, el formato general para indicar que queremos realizar la
		 * conversión es: (tipo) valor_a_convertir
		 * 
		 * En este ejemplo, si se sustituye la primera línea int num1=100 por int
		 * num1=1000000, el código compilaría bien, pero habría pérdida de datos, pues
		 * el 1000000 se sale del rango de short, que comprende desde -32768 a 32767. Al
		 * mostrar por consola el valor se obtendría un resultado incongruente.
		 */

		/* Conversión IMPLICITA */

		/*
		 * En este caso no se necesita escribir código para que la conversión se lleve a
		 * cabo. Ocurre cuando se realiza lo que se llama una conversión ancha (widening
		 * casting), es decir, cuando se coloca un valor pequeño en un contenedor
		 * grande.
		 * 
		 * Si al hacer la conversión de un tipo a otro se dan las 2 siguientes premisas:
		 * 
		 * Los dos tipos son compatibles.
		 * 
		 * El tipo de la variable destino es de un rango mayor al tipo de la variable
		 * que se va a convertir.
		 * 
		 * entonces, la conversión entre tipos es automática, ésto es, la máquina
		 * virtual de Java tiene toda la información necesaria para realizar la
		 * coversión sin necesidad de "ayuda" externa. A ésto se le conoce como
		 * conversión implícita y cuando hablamos de los tipos numéricos de Java se le
		 * da el nombre de widening conversion que traducido vendría a ser algo así como
		 * "conversión por ampliación". No obstante, no todos los tipos numéricos son
		 * compatibles con char o boolean (concretamente byte y short) y estos dos no lo
		 * son entre ellos.
		 */

		/*
		 * EJEMPLO UNO: int a double (conversión implícita [procedimiento automática])
		 */

		double division1;

		int numerador1 = 90;
		int denominador2 = 12;

		division1 = numerador1 / denominador2;

		System.out.println("La respuesta es (7.0):" + division1);

		/*
		 * EJEMPLO DOS: short a byte (conversión implícita [procedimiento automática])
		 */
		byte division2;

		short numerador2 = 8;
		short demominador2 = 4;

		division2 = (byte) (numerador2 / demominador2);
		System.out.println("La respuesta es (2):" + division2);

		/*
		 * EJEMPLO TRES: short a byte (conversión implícita [procedimiento automática])
		 */
		byte division3;

		short numerador3 = 9;
		short demominador3 = 3;

		division3 = (byte) (numerador3 / demominador3);
		System.out.println("La respuesta es (3):" + division3);

		/*
		 * EJEMPLO CUATRO: byte a short (conversión implícita [procedimiento
		 * automática])
		 */
		short division4;

		byte numerador4 = 9;
		byte demominador4 = 3;

		division4 = (short) (numerador4 / demominador4);
		System.out.println("La respuesta es (3):" + division4);

		/*
		 * Conversión de un tipo de dato menor hacia Un tipo de dato mayor byte es más
		 * pequeño que short
		 */
		byte miByte = 5;
		short miShort = miByte;

		short miShort2 = 5;
		byte miByte2 = (byte) miShort2;

		/*
		 * double numeroGrandeYDecimal = 1350000000.751422; int numeroMasPequenyoYEntero
		 * = numeroGrandeYDecimal;
		 */

		double numeroGrandeYDecimal = 1350000000.751422;
		int numeroMasPequenyoYEntero = (int) numeroGrandeYDecimal;

		// Ejemplo de Conversión Implícita
		char letraChar = 'k';
		int intByChar = letraChar;

		// Ejemplo de Conversión Implícita
		int numeroInt2 = 8664;
		long longByInt = numeroInt2;

		// Ejemplo de Conversión Implícita
		short numeroShort = 12500;
		double doubleByShort = numeroShort;

		// Ejemplo de Conversión Implícita
		char simboloChar = '¢';
		double doubleByChart = numeroShort;

		// Ejemplo de Casting
		short numeroShort2 = 32600;
		int numeroInt = (int) numeroShort2;

		// Ejemplo de Casting
		char letraChar2 = 'a';
		byte byteByChar = (byte) letraChar2;

		// Ejemplo de Casting
		long numeroLong = 1459888477;
		float longByfloat = (float) letraChar2;

		// Ejemplo de Casting
		double numeroDouble = 25.5;
		float numeroFloat = (float) numeroDouble;
		
		
		
		/*
		 * EJEMPLOS PRÁCTICO: CASTING
		 */
		
		/*
		 * Datos un número de palabras y un número de líneas por palabras, determine
		 * cuántas líneas puede formar con el número de palabras (teniendo en cuenta,
		 * que sobran palabras y esas conforman una nueva línea)
		 */
		System.out.println("Número de líneas: " + contarLineas(40, 10));
		
		/*
		 * EJEMPLOS PRÁCTICO: Conversión IMPLICITA
		 */
		
		/*
		 * Dato el caso que los productos tienen como precio sin iva como máximo 32700
		 * Escriba un código que calcule el precio tatal, teniendo el cuenta el IVA es del 19%
		 */
		short precioSinIva = 32650;
		System.out.println("Precio total del producto con precio " + 
		precioSinIva + ": " + costoMasIva(precioSinIva));

	}
	
	static int contarLineas(double numPal, double numLin) {
		// Al capturar número reales, tendré una devisión real
		double numFinal = numPal / numLin;
		// Aplico módulo del resultado, si es diferente de cero, 
		// es porque tiene parte decimal, si la tiene adiciono 1 al resultado
		if (numFinal % 1 != 0) {
			numFinal++;
		}
		//aplico casting para eliminar la parte decimal
		return (int) numFinal;
	}
	
	
	static double costoMasIva(short valor) {
		/*
		 * El precio que recibe es short,  porque la tienda tiene como precio máximo 32700
		 *
		 * Al recibir el valor, se guarda dentro un double  porque al sumar el 19% del precio, puede que
		 * éste sea mayor a 32700 y adicionalmente al calcular porcentaje, puede que nos dé un valor real
		 * No es necesario hacer casting, porque el valor es menor al contenedor
		 */
		double resultado = valor;
		resultado += resultado * 0.19;
		return resultado;
	}

}
