package Clase5;

public class EjercicioCiclos {

	public static void main(String[] args) {
		System.out.println("Pedirle que muestre la librería Math el Jueves");
		// hacer un triángulo rectángulo dependiendo de n
		// 1
		// 2 2
		// 3 3 3
		/*
		 * 
		 * Juan(0) Paola(1) Kennit(2) notas 4 6 6 notas 5 3 5 notas 3 4 2
		 */

		double estudiantesNotas[][] = new double[3][3];

		estudiantesNotas[0][0] = 4;
		estudiantesNotas[0][1] = 6;
		estudiantesNotas[0][2] = 6;

		estudiantesNotas[1][0] = 5;
		estudiantesNotas[1][1] = 3;
		estudiantesNotas[1][2] = 5;

		estudiantesNotas[2][0] = 3;
		estudiantesNotas[2][1] = 4;
		estudiantesNotas[2][2] = 2;

		System.out.println("--- SEPARACIÓN --- Figura UNO ---");
		figuraUno(4);

		System.out.println("--- SEPARACIÓN --- Figura DOS ---");

		// 1
		// 1 2
		// 1 2 3
		// 1 2 3 4
		figuraDos(10);

		System.out.println("--- SEPARACIÓN --- Figura TRES ---");

		// 4 3 2 1
		// 3 2 1
		// 3 2
		// 1
		figuraTres(10);

		System.out.println("--- SEPARACIÓN --- Figura CUATRO ---");

		// 1
		// 1 1 1
		// 1 1 1
		// 1

		// @
		// @ @ @
		// @ @ @ @ @
		// @ @ @
		// @
		figuraCuatro(5);

		System.out.println("Otro EJERCICIO");

		piramide(5);
	}

	public static void figuraUno(int tam) {
		for (int i = 1; i <= tam; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(i + " ");
			}
			System.out.println("");
		}
	}

	public static void figuraDos(int tam) {
		for (int i = 1; i <= tam; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println("");
		}
	}

	public static void figuraTres(int tam) {
		for (int i = tam; i >= 1; i--) {
			for (int j = i; j >= 1; j--) {
				System.out.print(j + " ");
			}
			System.out.println("");
		}
	}

	public static void figuraCuatro(int tam) {
		// For, dos for dentro del for
		// Si es la mitad del número
		// Ciclo para imprimir espacios
		// Ciclo para imprimir @

		System.out.println(Math.ceil(5 / 2));

		for (int i = 0; i < tam; i++) {
			for (int j = 0; j < tam; j++) {
				System.out.print("@ ");
			}

			for (int j = 0; j < tam; j++) {
				System.out.print("@ ");
			}
			System.out.println("");
		}
	}

	public static void piramide(int num) {
		int abajo = num;
		num = num / 2;
		for (int altura = 1; altura <= num; altura++) {
			// Espacios en blanco
			for (int blancos = 1; blancos <= num - altura; blancos++) {
				System.out.print(" ");
			}

			// Asteriscos
			for (int asteriscos = 1; asteriscos <= (altura * 2) - 1; asteriscos++) {
				System.out.print("@");
			}
			System.out.println();
		}
	}

}
