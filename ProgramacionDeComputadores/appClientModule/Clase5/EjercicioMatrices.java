package Clase5;

import java.util.Scanner;

public class EjercicioMatrices {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		int a, b, c, d;

		System.out.println("Ingrese Tamaños M1: ");

		System.out.print("a: ");
		a = in.nextInt();
		System.out.print("b: ");
		b = in.nextInt();

		int[][] matriz1 = new int[a][b];

		for (int i = 0; i < matriz1.length; i++) {
			for (int j = 0; j < matriz1[i].length; j++) {
				matriz1[i][j] = in.nextInt();
			}
		}

		System.out.println("Esto fue lo que ingresó en M1");

		for (int i = 0; i < matriz1.length; i++) {
			for (int j = 0; j < matriz1[i].length; j++) {
				System.out.print(matriz1[i][j] + " ");
			}
			System.out.println("");
		}

		System.out.println("Ingrese Tamaños M2: ");

		System.out.print("c: ");
		c = in.nextInt();
		System.out.print("d: ");
		d = in.nextInt();

		int[][] matriz2 = new int[c][d];

		for (int i = 0; i < matriz2.length; i++) {
			for (int j = 0; j < matriz2[i].length; j++) {
				matriz2[i][j] = in.nextInt();
			}
		}

		System.out.println("Esto fue lo que ingresó en M2");

		for (int i = 0; i < matriz2.length; i++) {
			for (int j = 0; j < matriz2[i].length; j++) {
				System.out.print(matriz2[i][j] + " ");
			}
			System.out.println("");
		}

	}

}
