package Clase5;

import java.util.Scanner;

public class Matrices090418 {

	public static void main(String[] args) {
		// Matrices

		/*
		 * 1 2 3 4 5 6 6 8 3
		 */

		// 1) Declaración
		double nota;
		double[] notas;
		double[][] notasMateria;

		// 2) Creación
		double notasMateria2[][] = new double[26][5];

		// 3) Inicialización
		// Manera UNA estrella (la anterior) y se le asignan los valores por defecto
		/*
		 * double, float: 0.0 int, long, short, byte: 0 boolean: false char: '\u0000'
		 * String: null
		 */

		// Manera DOS estrellas
		int[][] otrasNotasMateriaDos = { { 1, 2 }, { 4, 5 }, { 7, 8 } };
		// Manera TRES estrellas
		int[][] cuadrada = new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		// 4) Acceso
		notasMateria2[0][0] = 5.0;

		// return notasMateria2[0], retorna un arreglo que viene siendo la primera fila

		// 5) Recorrido

		// PRÁCTICA Y EJERCICIOS EN CLASE
		System.out.println("DETERMINAR LA TRANSPUESTA DE UNA MATRIZ");
		System.out.println("-- MATRIZ ORIGINAL --");
		for (int i = 0; i < otrasNotasMateriaDos.length; i++) {
			for (int j = 0; j < otrasNotasMateriaDos[i].length; j++) {
				System.out.print(otrasNotasMateriaDos[i][j] + " ");
			}
			System.out.println("");
		}

		int[][] respuesta;
		System.out.println("-- TRANSPUESTA --");

		respuesta = transpuesta(otrasNotasMateriaDos);
		for (int i = 0; i < respuesta.length; i++) {
			for (int j = 0; j < respuesta[i].length; j++) {
				System.out.print(respuesta[i][j] + " ");
			}
			System.out.println("");
		}

		System.out.println("");

		System.out.println("--- --- --- SEPARACIÓN --- --- ---");

		System.out.println("");

		System.out.println("DETERMINAR LA DIAGONAL PRIMERA");
		System.out.println("-- MATRIZ ORIGINAL --");
		for (int i = 0; i < cuadrada.length; i++) {
			for (int j = 0; j < cuadrada[i].length; j++) {
				System.out.print(cuadrada[i][j] + " ");
			}
			System.out.println("");
		}

		int salidaPrimaria[] = diagonalPrimera(cuadrada);
		System.out.println("-- ARREGLO DE LA DIAGONAL --");
		for (int i = 0; i < salidaPrimaria.length; i++) {
			System.out.print(salidaPrimaria[i] + " ");
		}
		System.out.println("\n");

		System.out.println("--- --- --- SEPARACIÓN (PRUEBA 1) --- --- ---");

		System.out.println("\n");

		System.out.println("DETERMINAR LA DIAGONAL SEGUNDARIA");
		System.out.println("-- MATRIZ ORIGINAL --");
		for (int i = 0; i < cuadrada.length; i++) {
			for (int j = 0; j < cuadrada[i].length; j++) {
				System.out.print(cuadrada[i][j] + " ");
			}
			System.out.println("");
		}

		int salidaSegundaria[] = diagonalSegundaria(cuadrada);
		System.out.println("-- ARREGLO DE LA DIAGONAL --");
		for (int i = 0; i < salidaSegundaria.length; i++) {
			System.out.print(salidaSegundaria[i] + " ");
		}

		System.out.println("\n");

		System.out.println("--- --- --- SEPARACIÓN (PRUEBA 1) --- --- ---");

		System.out.println("\n");

		// cuadrada[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; (¿Se puede re
		// inicializar?)

		int[][] cuadrada2 = new int[][] { { 1, 2, 3, 4 }, { 5, 5, 6, 7 }, { 8, 9, 9, 8 }, { 7, 6, 5, 4 } };

		System.out.println("DETERMINAR LA DIAGONAL SEGUNDARIA");
		System.out.println("-- MATRIZ ORIGINAL --");
		for (int i = 0; i < cuadrada2.length; i++) {
			for (int j = 0; j < cuadrada2[i].length; j++) {
				System.out.print(cuadrada2[i][j] + " ");
			}
			System.out.println("");
		}

		int salidaSegundaria2[] = diagonalSegundaria(cuadrada2);
		System.out.println("-- ARREGLO DE LA DIAGONAL --");
		for (int i = 0; i < salidaSegundaria2.length; i++) {
			System.out.print(salidaSegundaria2[i] + " ");
		}

	}

	public static int[][] transpuesta(int[][] a) {
		int[][] salida = new int[a[0].length][a.length];

		for (int i = 0; i < salida[0].length; i++) {
			for (int j = 0; j < salida.length; j++) {
				salida[j][i] = a[i][j];
			}
		}

		return salida;
	}

	public static boolean esCuadrada(int[][] m) {
		return m.length == m[0].length;
	}

	public static int[] diagonalPrimera(int[][] m) {
		if (esCuadrada(m)) {
			int dp[] = new int[m.length];

			for (int i = 0; i < dp.length; i++) {
				dp[i] = m[i][i];
			}

			return dp;

		} else {
			return new int[0];
		}
	}

	// P: 00, 11, 22
	// S: 20, 11, 02 (F: 2 1 0) (C: 0 1 2)

	// 0 1 2
	// 0 1 2 3
	// 1 4 5 6
	// 2 7 8 9
	public static int[] diagonalSegundaria(int[][] m) {
		if (esCuadrada(m)) {
			int dp[] = new int[m.length];
			int j = 0;
			for (int i = dp.length - 1; i >= 0; i--) {
				dp[i] = m[i][j];
				j++;
			}

			return dp;

		} else {
			return new int[0];
		}
	}

	/*
	 * 
	 * 
	 * 
	 * // P: 00, 11, 22 // S: 20, 11, 02 (F: 2 1 0) (C: 0 1 2)
	 * 
	 * Concluimos que no se puede porque al ejecutarse no se encontró la forma de
	 * incrementar las columnas 2 – 0 1 – 1
	 * 
	 * // 0 1 2
	 * 
	 * // 0 1 2 3 // 1 4 5 6 // 2 7 8 9 public static int[]
	 * diagonalSegundaria(int[][] m) { if (esCuadrada(m)) { int dp[] = new
	 * int[m.length];
	 * 
	 * for (int i = dp.length - 1; i >= 0; i--) { System.out.println(i + " – " +
	 * ((dp.length - 1) - i)); dp[i] = m[i][i - (dp.length - 1)]; }
	 * 
	 * return dp;
	 * 
	 * } else { return new int[0]; } }
	 * 
	 */

}
