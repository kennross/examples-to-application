package Clase5;

public class Ciclos {

	public static void main(String[] args) {
		// mdc(a,b): b si a % b = 0; mdc(b,a % b) si a % b != 0

		System.out.println("MCD (92, 26): " + calcularMDC(92, 26));
		System.out.println("MCD (72, 16): " + calcularMDC(72, 16));
		System.out.println("MCD (24, 4): " + calcularMDC(24, 4));
		System.out.println("MCD (16, 72): " + calcularMDC(16, 72));

		System.out.println("- - - - - - OTRO - - - - - - - ");

		System.out.println("3 es primo?: " + esPrimoWhile(3));
		System.out.println("6 es primo?: " + esPrimoFor(6));

		System.out.println("- - - - - - OTRO - - - - - - - ");

		System.out.println("Factorial (5): " + factorial(5));
		System.out.println("Factorial con While (5): " + factorialWhile(5));

		System.out.println("- - - - - - OTRO - - - - - - - ");
		System.out.println("Potencia de 5: " + potencia(5, 6));
		
		System.out.println("- - - - - - OTRO - - - - - - - ");
		System.out.println("Encuentro (2, 3, 4): " + calcularEncuentro(2, 3, 4));
		System.out.println("Encuentro (3, 4, 5): " + calcularEncuentro(3, 4, 5));
		System.out.println("Encuentro (8, 12, 30): " + calcularEncuentro(8, 12, 30));
		System.out.println("Encuentro (9, 12, 48): " + calcularEncuentro(9, 12, 48));
	}

	// Calcular a^b para b > 0 (sin usar la función pow).

	// 5^6, 5 * 5 * 5 * 5 * 5 * 5 (a = 5, b = 6)
	static int potencia(int a, int b) {
		int resultado = 1;
		for (int i = 0; i < b; i++) {
			resultado = resultado * a;
		}
		return resultado;
	}

	// . Ana viene al Poli cada A días, Bernardo cada B días, y Carlos cada C días.
	// Si todos vinieron al Poli hoy, ¿dentro de cuántos días vendrán nuevamente los
	// tres?

	// A = 1, B = 2, C = 3
	// A = 3, B = 4, C = 5
	// A = 3, B = 4, C = 6

	static int calcularEncuentro(int a, int b, int c) {
		boolean buscarDia = true;		
		int i = 0;
		
		while (buscarDia) {
			++i;
			if (i % a == 0 && i % b == 0 && i % c == 0) {
				buscarDia = false;
			}
		}
		return i;
	}

	// hacer un triángulo rectángulo dependiendo de n
	// 1
	// 2 2
	// 3 3 3
	// 4 4 4 4

	// también hacer
	// 1
	// 1 2
	// 1 2 3
	// 1 2 3 4

	// 4 3 2 1
	// 3 2 1
	// 3 2
	// 1

	// 1
	// 1 1 1
	// 1 1 1
	// 1

	public static int calcularMDC(int a, int b) {
		int temp;

		while (a % b != 0) {
			temp = b;
			b = a % b;

			a = temp;
		}

		return b;
	}

	public static boolean esPrimoWhile(int n) {
		int i = 2;
		while (i < n / 2) {
			if (n % i == 0) {
				return false;
			}
			i++;
		}
		return true;
	}

	public static boolean esPrimoFor(int n) {
		for (int i = 2; i < n / 2; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static int factorial(int n) {
		int factorial = 1;
		for (int i = n; i > 1; i--) {
			factorial *= i;
		}

		return factorial;
	}

	public static int factorialWhile(int n) {
		int factorial = 1;
		int i = n;

		while (i > 1) {
			factorial *= i;
			i--;
		}

		return factorial;
	}
}
