package Clase5;

public class JaqueCaballoRey {

	public static void main(String[] args) {
		
		/* Ejemplos PRESENTACIÓN: TIPOS DE DATOS, CONVERSIÓN IMPLÍCITA Y CASTING. */
		
		/*
		 * Declaración e inicialización de una
		 * cadena de caracteres
		 */
		String nombre = "Kennit David Ruz Romero";
		
		String parrafoLatin = "Lorem ipsum dolor sit amet," + 
				"tempor incididunt ut labore et dolore\n" + 
				"quis nostrud exercitation ullamco" + 
				"consequat. Duis aute irure dolor" + 
				"cillum dolore eu fugiat nulla pariatur.";

		String saludo = "Hola, Bienvenido: " + nombre;
		
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo lógico (booleanda)
		 */
		
		boolean isAdmin = true;
		boolean descuento = false;
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo char (carácter)
		 */

		char inicialNombre = 'K';
		char simboloInfinido = '∞';
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo byte (entero)
		 */
		
		byte edad = 20;
		byte temperaturaMinima = -70;
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo short (entero)
		 */

		short kilometrosCaminadosAnual = 32000;
		short balanceFinal = -29300;
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo int (entero)
		 */

		int montoMaximoPorTransaccion = 2100000;
		int balanceEnDolares = -1500000;
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo long (entero)
		 */

		long numeroDeHabitantesLatinoAmerica = 735000000;
		long balanceCredito = -725005400;
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo float (decimal)
		 */ //
		float promedioMantenerBeca = 3.8f;
		float valorPI = 3.14159265359f;
		float momentoMagnetico = -1.00115965218111f;
		
		/*
		 * Declaración e inicialización
		 * de una variable de tipo double (decimal)
		 */

		double dineroPreciso = 735000000745676543456568.78;
		double interesesMora = 650000000000.83635845;

		/* GENERAL */
		/*
		 * Un casting es una operación especial que nos permite realizar una conversión
		 * entre tipos
		 */

		/* CASTING */

		/*
		 * En el casting explícito sí es necesario escribir código. Ocurre cuando se
		 * realiza una conversión estrecha (narrowing casting), es decir, cuando se
		 * coloca un valor grande en un contenedor pequeño. Son susceptibles de pérdida
		 * de datos.
		 * 
		 * El casting es un procedimiento para transformar una variable primitiva de un
		 * tipo a otro.
		 * 
		 * Existen dos tipos de errores al hacer CASTING 
		 * - Lo está haciendo mal: Type mismatch: cannot convert from (dato1) to (dato2) 
		 * - No se puede hacer dicha conversión : Cannot cast from (dato1) to (dato2)
		 * - No es posible realizar casting entre una variable primitiva  booleana y cualquier otra variable primitiva.
		 * - Sí es posible realizar casting entre una variable primitiva char y una variable primitiva que almacene enteros
		 * 
		 * Como se ve, el formato general para indicar que queremos realizar la
		 * conversión es: (tipo) valor_a_convertir
		 * 
		 * En este ejemplo, si se sustituye la primera línea int num1=100 por int
		 * num1=1000000, el código compilaría bien, pero habría pérdida de datos, pues
		 * el 1000000 se sale del rango de short, que comprende desde -32768 a 32767. Al
		 * mostrar por consola el valor se obtendría un resultado incongruente.
		 */

		int codigo = 97;

		char codigoASCII = (char) codigo;

		System.out.println(codigoASCII);
		
		double num1 = 1;
		int resultado;
		
		//resultado = num1;
		
		int bin = 1;
		boolean exit;
		
		//exit = (boolean) 1;
		

		/* Conversión IMPLICITA */

		/*
		 * En este caso no se necesita escribir código para que la conversión se lleve a
		 * cabo. Ocurre cuando se realiza lo que se llama una conversión ancha (widening
		 * casting), es decir, cuando se coloca un valor pequeño en un contenedor
		 * grande.
		 * 
		 * Si al hacer la conversión de un tipo a otro se dan las 2 siguientes premisas:
		 * 
		 * Los dos tipos son compatibles.
		 * 
		 * El tipo de la variable destino es de un rango mayor al tipo de la variable
		 * que se va a convertir.
		 * 
		 * entonces, la conversión entre tipos es automática, ésto es, la máquina
		 * virtual de Java tiene toda la información necesaria para realizar la
		 * coversión sin necesidad de "ayuda" externa. A ésto se le conoce como
		 * conversión implícita y cuando hablamos de los tipos numéricos de Java se le
		 * da el nombre de widening conversion que traducido vendría a ser algo así como
		 * "conversión por ampliación". No obstante, no todos los tipos numéricos son
		 * compatibles con char o boolean (concretamente byte y short) y estos dos no lo
		 * son entre ellos.
		 */

		/*
		 * EJEMPLO UNO: int a double (conversión implícita [procedimiento automática])
		 */

		double division1;

		int numerador1 = 90;
		int denominador2 = 12;

		division1 = numerador1 / denominador2;

		System.out.println("La respuesta es (7.0):" + division1);

		/*
		 * EJEMPLO DOS: short a byte (conversión implícita [procedimiento automática])
		 */
		byte division2;

		short numerador2 = 8;
		short demominador2 = 4;

		division2 = (byte) (numerador2 / demominador2);
		System.out.println("La respuesta es (2):" + division2);

		/*
		 * En el ejemplo TRES y CUATRO, encontramos una excepción, teniendo en cuenta la
		 * definición, short es más grande que byte Es decir, en teoría, si quisiera
		 * pasar un número tipo short a byte No debería hacer conversión explícita
		 * (CASTING) porque la variable de destino es más grande, pero aquí es donde
		 * aparece la excepción, SÍ debe hacerse.
		 */

		/*
		 * EJEMPLO TRES: short a byte (conversión implícita [procedimiento automática])
		 */
		byte division3;

		short numerador3 = 9;
		short demominador3 = 3;

		division3 = (byte) (numerador3 / demominador3);
		System.out.println("La respuesta es (3):" + division3);

		/*
		 * EJEMPLO CUATRO: byte a short (conversión implícita [procedimiento
		 * automática])
		 */
		short division4;

		byte numerador4 = 9;
		byte demominador4 = 3;

		division4 = (short) (numerador4 / demominador4);
		System.out.println("La respuesta es (3):" + division4);
		
		/*
		 * Conversión de un tipo de dato menor
		 * hacia
		 * Un tipo de dato mayor
		 * byte es más pequeño que short
		 */
		byte miByte = 5;
		short miShort = miByte;
		
		short miShort2 = 5;
		byte miByte2 = (byte) miShort2;
		
		
		
		
		
		/* Seguir mirando aquí: https://sites.google.com/site/pro012iessanandres/java/conversion-entre-tipos-primitivos-casting */
		
		/*
		double numeroGrandeYDecimal = 1350000000.751422;
		int numeroMasPequenyoYEntero = numeroGrandeYDecimal;
		*/
		
		double numeroGrandeYDecimal = 1350000000.751422;
		int numeroMasPequenyoYEntero = (int) numeroGrandeYDecimal;
		
		// Ejemplo de Conversión Implícita
		char letraChar = 'k';
		int intByChar = letraChar;
		
		// Ejemplo de Conversión Implícita
		int numeroInt2 = 8664;
		long longByInt = numeroInt2;
		
		// Ejemplo de Conversión Implícita
		short numeroShort = 12500;
		double doubleByShort = numeroShort;
		
		// Ejemplo de Conversión Implícita
		char simboloChar = '¢';
		double doubleByChart = numeroShort;
		
		// Ejemplo de Casting
		short numeroShort2 = 32600;
		int numeroInt = (short) numeroShort2;
		
		// Ejemplo de Casting
		char letraChar2 = 'a';
		byte byteByChar = (byte) letraChar2;
		
		// Ejemplo de Casting
		long numeroLong = 1459888477;
		float longByfloat = (float) letraChar2;
		
		// Ejemplo de Casting
		double numeroDouble = 25.5;
		float numeroFloat = (float) numeroDouble;
		
		
		
		
		
		

		
		

		/*
		 * Datos los ángulos internos de un triángulo, determine: - Si forma un
		 * triángulo - Si puede formar un triángulo - y determine si es obtusángulo,
		 * rectángulo p acutángulo
		 */
		System.out.println("Ángulos (30, 90, 60): " + determinarTriangulo(30, 90, 60));
		System.out.println("Ángulos (25, 95, 60): " + determinarTriangulo(25, 95, 60));
		System.out.println("Ángulos (40, 80, 60): " + determinarTriangulo(40, 80, 60));

		/*
		 * Dado un número entero n, devuelva su valor absoluto
		 */
		System.out.println("Valor absoluto (5) (SALIDA: 5): " + calcularValorAbsoluto(5));
		System.out.println("Valor absoluto (-5) (SALIDA: 5): " + calcularValorAbsoluto(-5));

		/*
		 * Dado tres puntos sobre el plano cartesiano, determine si son colineales o no
		 */
		System.out.println(sonColineales(1, 1, 2, 2, 3, 3)
				? "Son colineales (puntos: x1 = 1, y1 = 1, x2 = 2, y2 = 2, x3 = 3, y3 = 3)"
				: "No lo son");
		System.out.println(sonColineales(4, 2, 6, 2, 9, 3)
				? "Son colineales (puntos: x1 = 4, y1 = 2, x2 = 6, y2 = 2, x3 = 9, y3 = 3) "
				: "No lo son");

		/*
		 * Datos un número de palabras y un número de líneas por palabras, determine
		 * cuántas líneas puede formar con el número de palabras (teniendo en cuenta,
		 * que sobran palabras y esas conforman una nueva línea)
		 */
		System.out.println("Número de líneas: " + contarLineas(40, 10));

		/*
		 * Datos en una matriz de 8 X 8: La fila y la columna de un caballo La fila y la
		 * columna del Rey contrario
		 * 
		 * Determine si el caballo le está haciendo jaque al Rey
		 */
		System.out.println(jaque(2, 7, 3, 5) ? "JAQUE" : "Sigue intentando");

	}

	static boolean jaque(int columnaCaballo, int filaCaballo, int columnaRey, int filaRey) {
		// Si comtemplamos todas las posibilidades de movimiento (FILA Y COLUMNA) del
		// caballo, alguna llega a ser igual a las del rey
		// LE HICE JAQUE

		// (-2F . +1C) (-2F . -1C) (+2F . +1C) (+2F . -1C)

		// (-2C . +1F) (-2C . -1F) (+2C . +1F) (+2C . -1F)

		// TEST
		System.out.println(columnaCaballo + 1);
		System.out.println(filaCaballo - 2);

		System.out.println("R_COLUMA: " + columnaRey + ", " + "R_FILA: " + filaRey);

		return (filaCaballo - 2 == filaRey && columnaCaballo + 1 == columnaRey)
				|| (filaCaballo - 2 == filaRey && columnaCaballo - 1 == columnaRey)
				|| (filaCaballo + 2 == filaRey && columnaCaballo + 1 == columnaRey)
				|| (filaCaballo + 2 == filaRey && columnaCaballo - 1 == columnaRey) ||

				(columnaCaballo - 2 == columnaRey && filaCaballo + 1 == filaRey)
				|| (columnaCaballo - 2 == columnaRey && filaCaballo - 1 == filaRey)
				|| (columnaCaballo + 2 == columnaRey && filaCaballo + 1 == filaRey)
				|| (columnaCaballo + 2 == columnaRey && filaCaballo - 1 == filaRey);
	}

	static boolean esMayorDeEdad(int edad) {
		boolean respuesta = true;

		if (edad >= 18) {
			respuesta = true;
		} else if (edad == 19) {
			respuesta = false;
		}

		return respuesta;
	}

	static int contarLineas(double numPal, double numLin) {
		// Al capturar número reales, tendré una devisión real
		double numFinal = numPal / numLin;
		// Aplico módulo del resultado, si es diferente de cero, 
		// es porque tiene parte decimal, si la tiene adiciono 1 al resultado
		if (numFinal % 1 != 0) {
			numFinal++;
		}
		//aplico casting para eliminar la parte decimal
		return (int) numFinal;
	}

	static int mayor(int a, int b, int c) {
		if (a > b) {
			if (a > c) {
				return a;
			} else {
				return c;
			}
		} else {
			if (b > c) {
				return b;
			} else {
				return c;
			}
		}
	}

	static boolean sonColineales(double x1, double y1, double x2, double y2, double x3, double y3) {
		/*
		 * Para determinar sin tres puntos en un plano son colineales, se obtiene la
		 * pendiente entre punto A y B, y con el punto B y C, si éstas son iguales, son
		 * colineales.
		 */

		double pendiente1_2 = (x2 - x1) / (y2 - y1);

		double pendiente2_3 = (x3 - x2) / (y3 - y2);

		// System.out.println("Pendiente 1:" + pendiente1_2 + ", Pendiente 2: " +
		// pendiente2_3);

		return pendiente1_2 == pendiente2_3;
	}

	static int calcularValorAbsoluto(int numero) {
		if (numero < 0) {
			return numero * -1;
		}

		return numero;
	}

	static String determinarTriangulo(double an1, double an2, double an3) {
		if (an1 + an2 + an3 == 180) {
			if (an1 > 90 || an2 > 90 || an3 > 90) {
				return "Obtusángulo";
			} else if (an1 == 90 || an2 == 90 || an3 == 90) {
				return "Rectángulo";
			} else {
				return "Acutángulo";
			}
		} else {
			return "No se puede formar un triángulo";
		}
	}
	
	static double costoMasIva(short valor) {
		double resultado = valor;
		resultado += resultado * 0.19;
		return resultado;
	}
}
