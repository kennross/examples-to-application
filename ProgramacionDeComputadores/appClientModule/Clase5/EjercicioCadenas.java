package Clase5;

public class EjercicioCadenas {

	public static void main(String[] args) {
		System.out.println("-- PRIMER EJERCICIO --");
		/*
		 * Dada una cadena, retornar el número de letras 'a' que tiene
		 */
		System.out.println("Politécnico Grancolombiano: " + countLetters('o', "Politécnico Grancolombiano"));
		System.out.println("Universidad: " + countLetters('a', "Universidad"));
		
		System.out.println("-- SEGUNDO EJERCICIO --");

		/*
		 * Dada una cadena que contiene solamente letras minusculas y sin carácteres
		 * especiales, retorne la misma cadena en mayúscula
		 */
		System.out.println("kennit: " + toUpperCase("kennit"));
		
		System.out.println("-- TERCER EJERCICIO --");

		/*
		 * Dadas dos cadenas de caracteres a y b, censume b dentro de a, poniendo tantos
		 * asteriscos como carxteres tenga b
		 */		

		System.out.println(censurar("kennit hola kennit", "hola"));
		System.out.println(censurar("kennit hola kennit", "halo"));
		System.out.println(censurar("hola", "la"));
		System.out.println(censurar("hola", "al"));
		
		System.out.println("-- CUARTO EJERCICIO --");

		/*
		 * Anagrama
		 */
		System.out.println("Anagrama: " + anagrama("pepe", "hola"));
		System.out.println("Anagrama: " + anagrama("omar", "roma"));
		System.out.println("Anagrama: " + anagrama("kennit", "nathalia"));
	}

	public static int countLetters(char letter, String chain) {
		int i = 0;

		for (int j = 0; j < chain.length(); j++) {
			if (chain.charAt(j) == letter)
				i++;
		}

		return i;
	}

	public static String toUpperCase(String chain) {
		String letterUpperCase = "";

		int tempLetterInt;
		for (int i = 0; i < chain.length(); i++) {
			tempLetterInt = (int) chain.charAt(i);
			letterUpperCase += (char) (tempLetterInt - 32);
		}

		return letterUpperCase;
	}
	
	public static String censurar(String a, String b) {
		String x = "";
		for (int i = 0; i < b.length(); i++) {
			x += "*";
		}
		
		return a.replaceAll(b, x);
	}

	public static boolean anagrama(String first, String second) {
		if (first.length() == second.length()) {
			int count = 0;

			for (int i = 0; i < first.length(); i++) {
				if (inLetter(first.charAt(i), second)) {
					count++;
				}
			}
			
			if (count == second.length()) {
				return true;
			}
		}
		return false;
	}

	public static boolean inLetter(char letter, String chain) {
		for (int i = 0; i < chain.length(); i++) {
			if (chain.charAt(i) == letter)
				return true;
		}
		return false;
	}
}
