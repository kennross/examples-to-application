package Clase5;

public class Cadenas {

	public static void main(String[] args) {
		
		// Cadenas como arreglos de caracteres
		String x = "Poli";
		String y = "Técnico";
		
		String z = x + y;
		
		//syso= PoliTécnico
		
		/*
		 * 1) Declaración
		 */
		String s1, nombre, apellido, etc;
		
		/*
		 * 2) No hay creación como tal
		 */
		
		/*
		 * 3) Inicialización
		 */
		String ejemplo;
		
		ejemplo = "Esto es un ejemplo";
		
		/*
		 * 4) Acceso
		 */
		System.out.println(x.charAt(0));
		
		for (int i = 0; i < x.length(); i++) {
			System.out.print(x.charAt(i));
		}
		
		System.out.println(" – – – OTRA COSA - - - -");
		
		
		String cadenaPrueba = "Kennit David Ruz Romero";
		
		System.out.println("Sin Espacios: " + returnWithoutSpace(cadenaPrueba));
		
		String palabra = "reconocer";
		
		System.out.println("Polindrome (" + palabra + "): " + polindrome(palabra));
	}
	
	
	public static String returnWithoutSpace(String cadena) {
		String cadenaSalida = "";
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) != ' ') {
				cadenaSalida += cadena.charAt(i);	
			}			
		}
		return cadenaSalida;
	}
	
	public static boolean polindrome(String x) {
		for (int i = 0; i < x.length(); i++) {
			if (x.charAt(i) != x.charAt(x.length() - i - 1)) {
				return false;
			}
		}
		
		return true;
	}
}