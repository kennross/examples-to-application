package Clase5;

import java.util.Scanner;

public class Main {	

	public static void main(String[] args) {		
		
		Scanner in = new Scanner(System.in);
		
		double x;
		double y;
		
		double h;
		double k;
		
		double r;
		
		System.out.print("Ingrese el valor de X: ");
		x = in.nextDouble();
		
		System.out.print("Ingrese el valor de Y: ");
		y = in.nextDouble();
		
		System.out.print("Ingrese el valor del RADIO: ");
		r = in.nextDouble();
		
		System.out.print("Ingrese el valor de H: ");
		h = in.nextDouble();
		
		System.out.print("Ingrese el valor de K: ");
		k = in.nextDouble();
		
		if (Main.insideCircle(x, y, h, k, r)) {
			System.out.println("Está dentro");
		} else {
			System.out.println("No está dentro");
		}

	}
	
	public static boolean insideCircle(double x, double y, double h, double k, double radio) {		
		double resultado;
		
		resultado = (Math.pow((x - h), 2)) + (Math.pow((y - k), 2));
		
		return resultado <= Math.pow(radio, 2);	
	}	
}
