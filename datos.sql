SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE eda;

USE eda;

CREATE TABLE IF NOT EXISTS `contactenos` (
`idMensaje` int(11) NOT NULL,
  `nombreCompleto` varchar(55) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(55) COLLATE utf8_spanish2_ci NOT NULL,
  `asunto` varchar(55) COLLATE utf8_spanish2_ci NOT NULL,
  `mensaje` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='Formulario de mensaje de contáctenos';


CREATE TABLE IF NOT EXISTS `personas` (
`idPersona` int(11) NOT NULL,
  `nombres` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `edad` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='Tabla para ejemplo de aplicación de las técnicas Ajax, JSon, jQuery';


INSERT INTO `personas` (`idPersona`, `nombres`, `apellidos`, `edad`) VALUES
(1, 'Kennit David', 'Ruz Romero', 18);

ALTER TABLE `contactenos`
 ADD PRIMARY KEY (`idMensaje`);

ALTER TABLE `personas`
 ADD PRIMARY KEY (`idPersona`);

ALTER TABLE `contactenos`
MODIFY `idMensaje` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;

ALTER TABLE `personas`
MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;