var collection_song = [
{
	name: 'Do Not Eave Me',
	audio: new Audio('mp3s/do_not_eave_me.mp3')
},
{
	name: 'Free Fall',
	audio: new Audio('mp3s/free_fall.mp3')
},
{
	name: 'Heavy Weight',
	audio: new Audio('mp3s/heavyweight.mp3')
},
{
	name: 'Invincible',
	audio: new Audio('mp3s/invincible.mp3')
},
{
	name: 'My Heart',
	audio: new Audio('mp3s/my_heart.mp3')
}];

var index_song = 0;

function play() {	
	showNameSong();
	collection_song[index_song].audio.play();
}

function stop() {
	collection_song[index_song].audio.pause();
	collection_song[index_song].audio.currentTime = 0;
}

function next() {
	stop();
	index_song++;
	console.log(index_song);
	if (index_song < collection_song.length) {
		showNameSong();	
		play();	
	} else {
		console.log('No hay más canciones para la derecha')
		index_song--;
	}
	
}

function previous() {
	stop();
	index_song--;
	console.log(index_song);

	if (index_song > -1) {
		showNameSong();	
		play();
	} else {
		console.log('No hay más canciones para la izquierda')
		index_song++;
	}
	
	
}

function showNameSong() {
	$('#name_song').val(collection_song[index_song].name);
}