/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import entidades.Empleado;
import entidades.Pago;
import entidades.Reunion;
import entidades.Direccion;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author kennitromero
 */
public class Test {
    
    
    private static EntityManager manager;
    private static EntityManagerFactory emf;
    
    public static void main(String[] args) {
        emf = Persistence.createEntityManagerFactory("UnitPersistenceOne");
        
        manager = emf.createEntityManager();
        
        manager.getTransaction().begin();
        
        Empleado e1 = new Empleado(20L, "Kennit", "Romero", 
                new Direccion(Direccion.Tipo.Carrera, "22", "63C", 39));
        Pago pe11 = new Pago("Curso Uno", 40, LocalDateTime.now(), e1);
        Pago pe12 = new Pago("Horas Extras", 10, LocalDateTime.now(), e1);
        
        Empleado e2 = new Empleado(92L, "Pepito", "Pérez", 
                new Direccion(Direccion.Tipo.Carrera, "19Bis", "8", 19) );
        Pago pe21 = new Pago("Declaración de Renta", 40, LocalDateTime.now(), e2);
        Pago pe22 = new Pago("Nómina", 10, LocalDateTime.now(), e2);
        
        Reunion r = new Reunion("Fiesta de Fin de año", LocalDateTime.now());
        List<Empleado> empleados = new ArrayList();
        
        empleados.add(e1);
        empleados.add(e2);
        
        r.setEmpleados(empleados);
        
        
        manager.persist(e1);
        manager.persist(pe11);
        manager.persist(pe12);
        
        manager.persist(e2);
        manager.persist(pe21);
        manager.persist(pe22);
        
        manager.persist(r);
        
        manager.getTransaction().commit();
    }    
}
