package entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author kennitromero
 */
@Entity
@Table(name = "EMPLEADOS")
public class Empleado implements Serializable {    
    
    @Id
    @Column(name = "CEDULA")
    private Long id;
    
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Column(name = "APELLIDO")
    private String apellido;
    
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "SEGURIDAD_ID")
    private Direccion seguridadSocial;     
    
    @OneToMany(mappedBy = "empleado")    
    private List<Pago> pagos;
    
    @ManyToMany(mappedBy = "empleados")
    private List<Reunion> reuniones;

    public Empleado() {
    }

    public Empleado(Long id, String nombre, String apellido, Direccion seguridadSocial) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.seguridadSocial = seguridadSocial;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Direccion getSeguridadSocial() {
        return seguridadSocial;
    }

    public void setSeguridadSocial(Direccion seguridadSocial) {
        this.seguridadSocial = seguridadSocial;
    }

    public List<Pago> getPagos() {
        return pagos;
    }

    public void setPagos(List<Pago> pagos) {
        this.pagos = pagos;
    }        

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", nombre=" + nombre + ", apellido=" 
                + apellido + ", seguridadSocial=" + seguridadSocial + '}';
    }    

    public List<Reunion> getReuniones() {
        return reuniones;
    }

    public void setReuniones(List<Reunion> reuniones) {
        this.reuniones = reuniones;
    }    
}
