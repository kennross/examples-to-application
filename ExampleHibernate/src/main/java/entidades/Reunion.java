/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author kennitromero
 */
@Entity
@Table(name = "REUNIONES")
public class Reunion implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "REUNION_ID")
    private Long id;
    
    @Column(name = "TITULO")
    private String titulo;
    
    @Column(name = "FECHA")
    private LocalDateTime fecha;
    
    @ManyToMany
    @JoinTable(name="EMPLEADOS_REUNIONES")
    private List<Empleado> empleados;

    public Reunion() {
    }

    public Reunion(String titulo, LocalDateTime fecha) {
        this.titulo = titulo;
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }        

    @Override
    public String toString() {
        return "Reunion{" + "id=" + id + ", titulo=" + titulo + ", fecha=" 
                + fecha + '}';
    }
}
