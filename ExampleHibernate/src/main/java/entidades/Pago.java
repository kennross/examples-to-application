/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author kennitromero
 */
@Entity
@Table(name = "PAGOS")
public class Pago implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "PAGO_ID")
    private Long id;
    
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Column(name = "HORAS_TRABAJADAS")
    private Integer horasTrabajadas;
    
    @Column(name = "FECHA_PAGO")
    private LocalDateTime fechaPago;
    
    @ManyToOne
    @JoinColumn(name = "CEDULA")
    private Empleado empleado;

    public Pago() {
    }

    public Pago(String descripcion, Integer horasTrabajadas, 
            LocalDateTime fechaPago, Empleado empleado) {        
        this.descripcion = descripcion;
        this.horasTrabajadas = horasTrabajadas;
        this.fechaPago = fechaPago;
        this.empleado = empleado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(Integer horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public LocalDateTime getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDateTime fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
      
    @Override
    public String toString() {
        return "Pago{" + "id=" + id + ", descripcion=" + descripcion 
                + ", horasTrabajadas=" + horasTrabajadas + ", fechaPago=" 
                + fechaPago + ", empleado=" + empleado + '}';
    }
}
