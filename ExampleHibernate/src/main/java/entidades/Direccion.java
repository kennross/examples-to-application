package entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author kennitromero
 */
@Entity
@Table(name = "DIRECCIONES")
public class Direccion implements Serializable {
    
    public enum Tipo { 
        Avenida, 
        AvenidaCalle, 
        AvenidaCarrera, 
        Calle, 
        Carrera, 
        Circular, 
        Cincunvalar, 
        Diagonal, 
        Manzana, 
        Transversal, 
        Via 
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "DIRECION_ID")
    private Long id;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO")
    private Tipo tipo;
    
    @Column(name = "NUMERO_UNO")
    private String numeroUno;
    
    @Column(name = "NUMERO_DOS")
    private String numeroDos;
    
    @Column(name = "GUION")
    private Integer guion;
        
    @OneToOne(mappedBy = "seguridadSocial")
    private Empleado empleado;

    public Direccion() {
    }

    public Direccion(Tipo tipo, String numeroUno, String numeroDos, Integer guion) {
        this.tipo = tipo;
        this.numeroUno = numeroUno;
        this.numeroDos = numeroDos;
        this.guion = guion;
    }        

    public Long getId() {
        return id;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public String getNumeroUno() {
        return numeroUno;
    }

    public void setNumeroUno(String numeroUno) {
        this.numeroUno = numeroUno;
    }

    public String getNumeroDos() {
        return numeroDos;
    }

    public void setNumeroDos(String numeroDos) {
        this.numeroDos = numeroDos;
    }

    public Integer getGuion() {
        return guion;
    }

    public void setGuion(Integer guion) {
        this.guion = guion;
    }        

    public void setId(Long id) {
        this.id = id;
    }    

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }        
}
