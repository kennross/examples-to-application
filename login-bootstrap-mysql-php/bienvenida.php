<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		Bienvenido
	</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<style>
		.margin {
			margin: 20px 0;
		}

		.btn {
			display: block;
			margin: 0 auto;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-4 col-xs-offset-4">
				<?php
				session_start();
				?>
				
				<div class="margin"></div>

				<div class="well">
					<div class="page-header">
						<h1 class="text-center">
							Bienvenido
							<small>
								<?php 
								echo $_SESSION['user']['email'];
								?>
							</small>
						</h1>
						<p>
							Ya existe una sesión, y ahora puedes cerrar sesión y volver al inicio con el siguiente botón
						</p>
						<a href="controlador.php?logout" class="btn btn-success">
							Cerrar Sesión
						</a>
					</div>
				</div>

				<?php if (isset($_SESSION['exit'])) { ?>

				<div class="margin"></div>

				<div class="alert alert-<?php echo $_SESSION['exit']['type'] ?> alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">
							&times;
						</span>
					</button>
					<?php echo $_SESSION['exit']['msj'] ?>
				</div>

				<?php } ?>

				<?php 
				$_SESSION['exit'] = null;
				?>
			</div>
		</div>
	</div>
</body>
</html>