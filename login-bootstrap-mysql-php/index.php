<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		Sistema de autentificación
	</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<style>
		.margin {
			margin: 20px 0;
		}

		.btn {
			display: block;
			margin: 0 auto;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-4 col-xs-offset-4">

				<div class="margin"></div>

				<?php 
				session_start(); 				
				?>

				<div class="well">
					<div class="page-header">
						<h1 class="text-center">
							Sistema de autentificación
						</h1>
						<p>
							Todos los sistemas necesitan de este sistema para llevar un seguimiento
						</p>
					</div>
					<h2>
						Datos de prueba
					</h2>
					<p>
						<b>
							Correo Electrónico:
						</b> 
						kennitromero@gmail.com

						<br>

						<b>
							Contraseña:
						</b> 
						12345
					</p>
				</div>
				<form action="controlador.php" method="POST">
					<div class="form-group">
						<label for="email">
							Correo Electrónico
						</label>
						<input type="email" required="true" 
						name="email" id="email" 
						placeholder="Ingrese su correo electrónico" 
						class="form-control"
						value="kennitromero@gmail.com">
					</div>
					<div class="form-group">
						<label for="password">
							Contraseña
						</label>
						<input type="password" required="true" 
						name="password" id="password" 
						placeholder="Ingrese su contraseña" 
						class="form-control" value="12345">
					</div>
					<button type="submit" class="btn btn-success">
						Iniciar Sesión
					</button>
				</form>

				<?php if (isset($_SESSION['exit'])) { ?>

				<div class="margin"></div>

				<div class="alert alert-<?php echo $_SESSION['exit']['type'] ?> alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">
							&times;
						</span>
					</button>
					<?php echo $_SESSION['exit']['msj'] ?>
				</div>

				<?php } ?>

				<?php 
				$_SESSION['exit'] = null;
				?>
			</div>
		</div>
	</div>
</body>
</html>