<?php 
session_start(); 
$_SESSION['exit'] = null;
$_SESSION['user'] = null;

try {
	$miConexion = new PDO('mysql:host=localhost;dbname=openhouse', 'root', '');	
	$consulta = $miConexion->prepare('SELECT * FROM `users`');
	$consulta->execute();

	$users = $consulta->fetchAll(PDO::FETCH_ASSOC);
	$user_current = null;

	foreach ($users as $u) {
		if ($u['email'] == $_POST['email']) {
			$user_current = $u;
		}
	}
	
	if ($user_current != null) {
		if ($user_current['password'] == $_POST['password']) {
			$_SESSION['user'] = $user_current;			
			header('Location: bienvenida.php');
		} else {
			$_SESSION['exit']['msj'] = 'Contraseña incorrecta';
			$_SESSION['exit']['type'] = 'danger';
			header('Location: index.php');
		}
	} else {
		$_SESSION['exit']['msj'] = 'Usuario no registrado, 
		por favor contacte a un administrador';
		$_SESSION['exit']['type'] = 'warning';
		header('Location: index.php');
	}
} catch (PDOException $e) {
	$_SESSION['exit']['msj'] = 'No se pudo realizar la conexión, detalle: ' . $e->getMessage();
	$_SESSION['exit']['type'] = 'danger';
	header('Location: index.php');
}


if (isset($_GET['logout'])) {
	$_SESSION['user'] = null;
	header('Location: index.php');
	$_SESSION['exit']['msj'] = 'Se ha cerrado sesión correctamente';
	$_SESSION['exit']['type'] = 'info';
}